
-- create view charity_donations_per_donation
CREATE OR REPLACE VIEW `charity_donations_per_donation` AS
SELECT
ccd.donor_id
, ccd.campaign_donation_id
, tc.wppx_posts_id as `charity_id`
, (tc.transfer_amount * tcid.fraction_distribution_money_amount) `amount` 
, invd.Investment_option_id `investment_option_id`
FROM `Transferred_charity` tc 
join `Transferred_charity_Investment_distribution` tcid on tc.id=tcid.transferred_charity_id
join `Investment_distribution` invd on tcid.investment_distribution_id=invd.id
join `wppx_charitable_campaign_donations` ccd on invd.wppx_charitable_campaign_donations_id = ccd.campaign_donation_id
order by ccd.donor_id, ccd.campaign_donation_id;

-- create view current_donation_worth
CREATE OR REPLACE VIEW current_donation_worth AS
SELECT donor_id, campaign_donation_id, (Invested_amount + cash_amount) * fraction_investment_option amount
FROM (select * from `Investment_option_valuation` order by refresh_datetime desc, id desc limit 1) iov
join `Investment_distribution` invd on invd.investment_option_id = iov.investment_option_id
join `wppx_charitable_campaign_donations` ccd on invd.wppx_charitable_campaign_donations_id = ccd.campaign_donation_id
join `Valuation_contribution` vc on vc.investment_distribution_id = invd.Id
join (select tr.id from  `Transferred` tr
      order by  datetime desc, tr.id desc limit 1) vctr  on vctr.Id = vc.Transferred_id 
order by iov.refresh_datetime desc, iov.id desc;

-- create view current_donation_profits
CREATE OR REPLACE VIEW current_donation_profits AS
SELECT ccd.donor_id
, ccd.campaign_donation_id
, ccd.campaign_id
, ccd.amount original_donation
, dpd.amount donated
, cdw.amount worth
, dpd.amount+cdw.amount-ccd.amount profit
FROM `wppx_charitable_campaign_donations` ccd
join (select campaign_donation_id, sum(amount) amount
      from charity_donations_per_donation 
      group by campaign_donation_id) dpd on ccd.campaign_donation_id = dpd.campaign_donation_id
join current_donation_worth cdw on ccd.campaign_donation_id = cdw.campaign_donation_id;
