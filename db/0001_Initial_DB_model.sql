-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 07, 2021 at 04:23 AM
-- Server version: 10.2.36-MariaDB-log-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `devtoekomstfonds_wp`
--

-- --------------------------------------------------------

--
-- Table structure for table `Investment_option`
--

CREATE TABLE IF NOT EXISTS `Investment_option` (
  `Id` int(11) NOT NULL,
  `description_short` varchar(200) DEFAULT NULL,
  `description_long` varchar(1000) DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `status` binary(1) NOT NULL,
  `name` varchar(50) NOT NULL,
  `fraction_to_reinvest` decimal(19,18) NOT NULL DEFAULT 0.500000000000000000
);

-- --------------------------------------------------------

--
-- Table structure for table `Investment_distribution`
--

CREATE TABLE IF NOT EXISTS `Investment_distribution` (
  `Id` int(11) NOT NULL,
  `WPPX_charitable_campaign_donations_Id` int(11) NOT NULL,
  `Investment_option_id` int(11) NOT NULL,
  `Transferred_Id` int(11) DEFAULT NULL,
  `fraction_distribution_donation` decimal(19,18) NOT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `Investment_option_valuation`
--

CREATE TABLE IF NOT EXISTS `Investment_option_valuation` (
  `Id` int(11) NOT NULL,
  `Investment_option_Id` int(11) NOT NULL,
  `Transferred_Id` int(11) DEFAULT NULL,
  `refresh_datetime` datetime NOT NULL,
  `Invested_amount` decimal(19,2) NOT NULL,
  `cash_amount` decimal(19,2) NOT NULL,
  `ideal_valuation` decimal(19,2) NOT NULL,
  `for_calculation` tinyint(1) NOT NULL DEFAULT 0
);

-- --------------------------------------------------------

--
-- Table structure for table `Posts_Charitable_campaign_donations`
--

CREATE TABLE IF NOT EXISTS `Posts_Charitable_campaign_donations` (
  `Id` int(11) NOT NULL,
  `WPPX_charitable_campaign_donations_Id` int(11) NOT NULL,
  `WPPX_posts_Id` int(11) NOT NULL,
  `fraction_post` decimal(19,18) NOT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `Transferred`
--

CREATE TABLE IF NOT EXISTS `Transferred` (
  `Id` int(11) NOT NULL,
  `Investment_option_Id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `calculated_transfer_amount` decimal(19,2) NOT NULL,
  `transfer_amount` decimal(19,2) NOT NULL,
  `old_cash_amount` decimal(19,2) NOT NULL,
  `new_cash_amount` decimal(19,2) NOT NULL,
  `new_invested_amount` decimal(19,2) NOT NULL,
  `old_invested_amount` decimal(19,2) NOT NULL,
  `status` enum('in','out') NOT NULL,
  `ideal_valuation` decimal(19,2) NOT NULL,
  `transfer_Id` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `Transferred_charity`
--

CREATE TABLE IF NOT EXISTS `Transferred_charity` (
  `Id` int(11) NOT NULL,
  `WPPX_Posts_Id` int(11) NOT NULL,
  `transfer_datetime` datetime DEFAULT NULL,
  `transfer_amount` decimal(19,2) NOT NULL,
  `status` enum('donated','not_donated') NOT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `Transferred_charity_Investment_distribution`
--

CREATE TABLE IF NOT EXISTS `Transferred_charity_Investment_distribution` (
  `Id` int(11) NOT NULL,
  `Investment_distribution_Id` int(11) NOT NULL,
  `Transferred_charity_id` int(11) NOT NULL,
  `fraction_distribution_money_amount` decimal(19,18) NOT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `Transferred_charity_Transferred`
--

CREATE TABLE IF NOT EXISTS `Transferred_charity_Transferred` (
  `Id` int(11) NOT NULL,
  `Transferred_charity_Id` int(11) NOT NULL,
  `Transferred_Id` int(11) NOT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `Valuation_contribution`
--

CREATE TABLE IF NOT EXISTS `Valuation_contribution` (
  `Id` int(11) NOT NULL,
  `Investment_distribution_Id` int(11) NOT NULL,
  `Transferred_id` int(11) NOT NULL,
  `fraction_investment_option` decimal(19,18) NOT NULL
);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Investment_option`
--
ALTER TABLE `Investment_option`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Investment_distribution`
--
ALTER TABLE `Investment_distribution`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Investment_distribution_1` (`WPPX_charitable_campaign_donations_Id`),
  ADD KEY `FK_Investment_distribution_2` (`Investment_option_id`),
  ADD KEY `FK_Investment_distribution_3` (`Transferred_Id`);

--
-- Indexes for table `Investment_option_valuation`
--
ALTER TABLE `Investment_option_valuation`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Investment_option_valuation_1` (`Investment_option_Id`),
  ADD KEY `FK_Investment_option_valuation_2` (`Transferred_Id`);

--
-- Indexes for table `Posts_Charitable_campaign_donations`
--
ALTER TABLE `Posts_Charitable_campaign_donations`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Posts_Charitable_campaign_donations_1` (`WPPX_charitable_campaign_donations_Id`),
  ADD KEY `FK_Posts_Charitable_campaign_donations_2` (`WPPX_posts_Id`);

--
-- Indexes for table `Transferred`
--
ALTER TABLE `Transferred`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Transferred_1` (`Investment_option_Id`);

--
-- Indexes for table `Transferred_charity`
--
ALTER TABLE `Transferred_charity`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Transferred_charity_WPPX_Posts` (`WPPX_Posts_Id`);

--
-- Indexes for table `Transferred_charity_Investment_distribution`
--
ALTER TABLE `Transferred_charity_Investment_distribution`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Transferred_charity_Investment_distribution_1` (`Investment_distribution_Id`),
  ADD KEY `FK_Transferred_charity_Investment_distribution_2` (`Transferred_charity_id`);

--
-- Indexes for table `Transferred_charity_Transferred`
--
ALTER TABLE `Transferred_charity_Transferred`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Transferred_charity_Transferred_1` (`Transferred_charity_Id`),
  ADD KEY `FK_Transferred_charity_Transferred_2` (`Transferred_Id`);

--
-- Indexes for table `Valuation_contribution`
--
ALTER TABLE `Valuation_contribution`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Valuation_contribution_1` (`Investment_distribution_Id`),
  ADD KEY `FK_Valuation_contribution_2` (`Transferred_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Investment_option`
--
ALTER TABLE `Investment_option`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Investment_distribution`
--
ALTER TABLE `Investment_distribution`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Investment_option_valuation`
--
ALTER TABLE `Investment_option_valuation`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Posts_Charitable_campaign_donations`
--
ALTER TABLE `Posts_Charitable_campaign_donations`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Transferred`
--
ALTER TABLE `Transferred`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Transferred_charity`
--
ALTER TABLE `Transferred_charity`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Transferred_charity_Investment_distribution`
--
ALTER TABLE `Transferred_charity_Investment_distribution`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Transferred_charity_Transferred`
--
ALTER TABLE `Transferred_charity_Transferred`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Valuation_contribution`
--
ALTER TABLE `Valuation_contribution`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;


-- start data

INSERT INTO `Investment_option` (`description_short`, `description_long`, `start_date`, `end_date`, `status`, `name`)
select 'short desc of green fund', 'long desc of green fund', '2020-11-01', NULL, 0x0, 'green fund'
from dual
where 'green fund' not in (select `name` from `Investment_option`);

INSERT INTO `Investment_option` (`description_short`, `description_long`, `start_date`, `end_date`, `status`, `name`)
select 'short desc of micro fund', 'long desc of micro fund', '2020-11-01', NULL, 0x0, 'micro fund'
from dual
where 'micro fund' not in (select `name` from `Investment_option`);

INSERT INTO `Investment_option` (`description_short`, `description_long`, `start_date`, `end_date`, `status`, `name`)
select 'short desc of responsible fund', 'long desc of responsible fund', '2020-11-01', NULL, 0x0, 'responsible fund'
from dual
where 'responsible fund' not in (select `name` from `Investment_option`);

INSERT INTO `Investment_option` (`description_short`, `description_long`, `start_date`, `end_date`, `status`, `name`)
select 'short desc of gov fund', 'long desc of gov fund', '2020-11-01', NULL, 0x0, 'gov fund'
from dual
where 'gov fund' not in (select `name` from `Investment_option`);

insert into Investment_option_valuation(Investment_option_id, refresh_datetime, Invested_amount,cash_amount,ideal_valuation,for_calculation)
select i.Id, now(), 0, 0, 0, 1
from Investment_option i
where i.Id not in (select Investment_option_Id from Investment_option_valuation);
