
-- Main donation query
SELECT
ccd.donor_id
, ccd.campaign_donation_id
, tc.wppx_posts_id as `Goed doel`
, tc.transfer_amount `Totale overmaking` 
, tcid.fraction_distribution_money_amount * tc.transfer_amount `Overmaking voor deze donatie`
, invd.Investment_option_id `Fonds`
, invd.fraction_distribution_donation * ccd.amount `Donatiedeel`
FROM `Transferred_charity` tc 
join `Transferred_charity_Investment_distribution` tcid on tc.id=tcid.transferred_charity_id
join `Investment_distribution` invd on tcid.investment_distribution_id=invd.id
join `wppx_charitable_campaign_donations` ccd on invd.wppx_charitable_campaign_donations_id = ccd.campaign_donation_id
order by ccd.donor_id, ccd.campaign_donation_id;


-- profit per goed doel
SELECT campaign_id, sum(original_donation), sum(donated), sum(worth), sum(profit)
FROM `current_donation_profits`
group by campaign_id;