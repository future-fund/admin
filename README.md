# Administrating investments and donations

Development of the IT section of the Future Fund that will extend the administration of donations and develop administration of investments.

This repository contains the following subdirectories:

* `/monitoring tools` - contains the scripts for monitoring tools originally developed by Thijs Aarts.
* `/db` - contains the DB scripts for the admin module.
* `/site` - contains the PHP code for the admin module.

