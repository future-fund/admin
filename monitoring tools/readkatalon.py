# dit script kijkt naar de katalon output bestanden en kijkt naar het tijd verschil tussen somige instructies en zet alle informatie in een grafiek
rundict ={}
uurdict ={}
errordict={}
counter=0
counter2=0
masterlogin=[]
masterplugin=[]
masternavigate=[]
navigatetijd=[]
navigatevalue=[]
masternavigate2=[]
navigatetijd2=[]
navigatevalue2=[]
import datetime
import time
import re
import statistics
import glob, os
import math
import matplotlib.pyplot as plt
from pathlib import Path
laatstetijd=''
timevalue2='ERROR'
imagelist = []
pathlist2 = Path('C:/Users/jackl/PycharmProjects/untitled/testmap/charts2').glob('**/*.png')
for path2 in pathlist2:
    path_in_str = str(path2)
    # print(path_in_str)
    image = path_in_str[56:93]
    imagelist.append(str(image))


print(imagelist)
pathlist = Path('C:/Users/jackl/PycharmProjects/untitled/testmap/katalonfiles').glob('**/*.log')
for path in pathlist:

    #bestand te openen
    #f = open('stdout-13.log', "r")
    path_in_str = str(path)
    file = path_in_str
    f = open(file, "r")
    bestand = f.read()
    #print(bestand)
    #split alle values om er een lijst van te maken
    bestandlist=bestand.split('2020')
    #de eerste tijd te selecteren zodat ik weet wanneer de monitoring begon
    timevalue=bestand[12:20]
    eerstetijd= datetime.datetime.strptime(timevalue, '%H:%M:%S')
    #print(timevalue)
    plugtijd=[]
    plugval=[]
    logintijd=[]
    loginvalue=[]
    timeval = datetime.datetime.strptime(timevalue, '%H:%M:%S')
    #print(type(timeval))
    #print(bestandlist)
    datum=bestand[1:11]#zodat ik weet wanneer de monitoring begon
    tijd = bestand[12:20]
    datum = datum + ':' + tijd
    datum = datum.replace(':', '-')
    namebestand = str(datum) + 'navtijd' + 'chart.png'
    print(namebestand)

    #print(datum,'dit is de datum')
    #print(bestand)
    for x in bestandlist[1:-1]:
        try:
            #selecteerd de datum
            timevalue2=x[7:15]
            #print('dit is de timevalue:',timevalue2)
            #print(x)
            #veranderd de tijd in een leesbare datum

            timeval2 = datetime.datetime.strptime(timevalue2, '%H:%M:%S')



            time_delta1 = (timeval2 - timeval) #berekent het tijd verschil tussen de tijd van deze iterarition en de vorige

            #print(time_delta1)
            total_seconds = time_delta1.total_seconds()# de tijd tussen de twee values in seconde
            #er was een error met de wisseling van dagen, dit was de makkelijkste manier om het op te lossen
            if total_seconds<0:
                total_seconds+=86400
            #print(total_seconds)
            #print(total_seconds)
            timeval=timeval2
            timeval3=str(timevalue2)
            timeval4=timeval3.strip(' ')
            #print(timeval4)
            #print(timeval4)
            laatstetijd=timeval2
            #deze twee regels zetten alle informatie in een list om in een grafiek te stoppen, ik heb specifiek deze informatie gekozen want het was de meest representatief van het laden van de website
            if "[MESSAGE][PASSED] - Navigate to 'https://dev.future.ngo/wp-admin/plugins.php' successfully" in x:
                plugtijd.append(timeval4)
                plugval.append(float(total_seconds))
                masterplugin.append(float(total_seconds))
            if "[MESSAGE][PASSED] - Object: 'Object Repository/Page_Login  Toekomstfonds/input_Onthoud mij_wp-submit' is clicked on" in x:
                logintijd.append(timeval4)
                loginvalue.append(float(total_seconds))
                masterlogin.append(float(total_seconds))
            if "[MESSAGE][PASSED] - Navigate to 'https://dev.future.ngo/wp-login.php' successfully" in x:
                navigatetijd2.append(timeval4)
                navigatevalue2.append(float(total_seconds))
                masternavigate2.append(float(total_seconds))
            if "[MESSAGE][PASSED] - Navigate to 'https://dev.future.ngo/nl' successfully" in x:

                navigatetijd.append(timeval4)
                navigatevalue.append(float(total_seconds))
                masternavigate.append(float(total_seconds))

            #deze regels kijkt of er een proces was dat langer dan 8 seconden duurde er print het process

                # dit is om te vinden hoeveel messages er zijn per uur

                uur = str(timeval2)[11:13]
                # uurdict[uur] +=1
                uurdict[uur] = uurdict.get(uur, 0) + 1
                if uur == '16':
                    counter += 1

            if total_seconds>8:

                print('waarschuwing')
                print('dit stuk duurt te lang')
                x2=x.strip('\n')
                print(x2)
                time_delta2 = (timeval2 - eerstetijd)
                total_seconds2=time_delta2.total_seconds()
                minuten=total_seconds2/60
                print('het duurde',total_seconds, 'en kwam',minuten,'minuten na dat het script begon')

                uur2 = str(timeval2)[11:13]
                print('dit is de uurvalue',uur2)
                # uurdict[uur] +=1
                errordict[uur2] = errordict.get(uur2, 0) + 1
                if uur2 == '16':
                    counter2 += 1
        except:
            print('er is een probleem met deze tijdvalue',timevalue2)

    #dit stukje tekst is om te bereken hoe lang het duurde per bestand om alle test uit te voeren
    hoelang=timeval2-eerstetijd



    rundict[datum]= hoelang
    if namebestand in imagelist:
        print('dit bestand is er al')
        continue
    #print(imagelist)

    #dit gedeelte hieronder is om grafieken te maken
    #print(len(loginvalue))
    #print(len(logintijd))
    #print(logintijd)
    strlengte = len(logintijd)
    lengforgraph = 0.89 * float(strlengte)
    lengforgraph= round(lengforgraph)
    #print('dit is de lengte', lengforgraph)
    if lengforgraph>655.36:
        lengforgraph=655.35


    fig = plt.figure(figsize=(lengforgraph, 10), dpi=80)
    # line 1 points
    x1 = logintijd
    y1 = loginvalue
    # plotting the line 1 points
    plt.plot(x1, y1, label='logintijd')

    # naming the x axis
    plt.xlabel('tijd')
    # naming the y axis
    plt.ylabel('tijd om in te loggen')
    # giving a title to my graph
    plt.title('dev.future.ngo inlogtijd')

    # show a legend on the plot
    plt.legend()

    # function to show the plot

    from matplotlib.pyplot import figure
    plt.show()
    #print('dit is de twee length for grep')
    fig.set_size_inches(lengforgraph, 10)
    datum=datum.replace(':','-')
    fig.savefig("C:/Users/jackl/PycharmProjects/untitled/testmap/charts2/"+str(datum)+'logintijd' + 'chart.png', dpi=100)

    #tweede keer voor de andere value

    #print(loginvalue)
    #print(plugval)
    strlengte = len(plugtijd)
    lengforgraph = 0.89 * float(strlengte)
    lengforgraph= round(lengforgraph)
    #print('dit is de lengte', lengforgraph)
    if lengforgraph>655.36:
        lengforgraph=655.35


    fig = plt.figure(figsize=(lengforgraph, 10), dpi=80)
    # line 1 points
    x1 = plugtijd
    y1 = plugval
    # plotting the line 1 points
    plt.plot(x1, y1, label='plugintijd')

    # naming the x axis
    plt.xlabel('tijd')
    # naming the y axis
    plt.ylabel('tijd om in te loggen')
    # giving a title to my graph
    plt.title('dev.future.ngo plugintijd')

    # show a legend on the plot
    plt.legend()

    # function to show the plot

    from matplotlib.pyplot import figure
    plt.show()
    #print('dit is de twee length for grep')
    fig.set_size_inches(lengforgraph, 10)
    #print(datum)
    datum=datum.replace(':','-')
    fig.savefig("C:/Users/jackl/PycharmProjects/untitled/testmap/charts2/"+str(datum)+'plugintijd' + 'chart.png', dpi=100)
    print('bestand',datum+'plugintijd.chart.png', 'is aangemaakt')


    # derde keer voor de andere value

    # print(loginvalue)
    # print(plugval)
    strlengte = len(navigatetijd)
    lengforgraph = 0.89 * float(strlengte)
    lengforgraph = round(lengforgraph)
    # print('dit is de lengte', lengforgraph)
    if lengforgraph > 655.36:
        lengforgraph = 655.35

    fig = plt.figure(figsize=(lengforgraph, 10), dpi=80)
    # line 1 points
    x1 = navigatetijd
    y1 = navigatevalue
    # plotting the line 1 points
    plt.plot(x1, y1, label='plugintijd')

    # naming the x axis
    plt.xlabel('tijd')
    # naming the y axis
    plt.ylabel('tijd om te navigeren')
    # giving a title to my graph
    plt.title('dev.future.ngo navigatietijd')

    # show a legend on the plot
    plt.legend()

    # function to show the plot

    from matplotlib.pyplot import figure

    plt.show()
    # print('dit is de twee length for grep')
    fig.set_size_inches(lengforgraph, 10)
    # print(datum)
    datum = datum.replace(':', '-')
    fig.savefig("C:/Users/jackl/PycharmProjects/untitled/testmap/charts2/" + str(datum) + 'navtijd' + 'chart.png',
                dpi=100)
    print('bestand', datum + 'navigatie.chart.png', 'is aangemaakt')


import collections
rundicttijd=[]
rundictvalues=[]

errordicttijd=[]
errordictvalues=[]

cntr=0
orundict = collections.OrderedDict(sorted(rundict.items()))
for x,y in orundict.items():
    cntr += 1
    #print(x,y)
    rundicttijd.append(str(cntr))

    rundictvalues.append(float(y.seconds))

x1 = rundicttijd
y1 = rundictvalues
# plotting the line 1 points
plt.plot(x1, y1, label='tijd van de dag')

# naming the x axis
plt.xlabel('datum')
# naming the y axis
plt.ylabel('')
# giving a title to my graph
plt.title('tijd om 200 loops uit te voeren')

# show a legend on the plot
plt.legend()

# function to show the plot

from matplotlib.pyplot import figure

#print('dit is de twee length for grep')
#print(datum)

plt.savefig("totaalchart.png")
#plt.show()


for x, y in uurdict.items():
    print(x, y)
    t = 'y'

for x, y in errordict.items():
    print(x, y)
    t = 'y'

for i in range(24):
    try:
        print()
        print(uurdict[str(i)] , errordict[str(i)])
        print(uurdict[str(i)]/errordict[str(i)])
    except:
        print('dit is de probleemunit',i)
#print('counter',counter)
#print('counter2',counter2)
#print(masterlogin)



# dit is de login tijd


login97=round(len(masterlogin)/100 *97)
print('dit is hoeveel entries er zijn',len(masterlogin))
#print(login97)

def information(mastervalues,namevalue):
    mastervalues.sort()
    lengte=len(mastervalues)
    rest97percent=math.ceil(lengte/100*97)
    rest99percent=math.ceil(lengte/100*99)
    rest95percent=math.ceil(lengte/100*95)
    percentile97=math.floor(lengte/100)
    act97percentile=mastervalues[rest97percent:rest97percent+percentile97]
    act95percentile=mastervalues[rest95percent:rest95percent+percentile97]
    act99percentile = mastervalues[rest99percent:rest99percent + percentile97]
    ninty=mastervalues[:login97]
    threety=mastervalues[login97:]
    averagel = sum(mastervalues) / len(mastervalues)
    average2 = sum(ninty) / len(ninty)
    average3 = sum(threety)/len(threety)
    average4=sum(act97percentile)/percentile97
    average5=sum(act95percentile)/percentile97
    average6=sum(act99percentile)/percentile97
    print(namevalue)
    print(act97percentile)
    print(act95percentile)
    print(act99percentile)
    print('dit is de average van',namevalue,'tijd',averagel)
    print('dit is de avereage ',namevalue,'tijd 97%',average2)
    print('dit zijn de ergste 3%',average3)
    print('dit is de 97 percentile',average4)
    print('dit is de 95 percentile', average5)
    print('dit is de 99 percentile',average6)
    print('\n')


information(masterlogin,'logging')
information(masterplugin,'plugin')
information(masternavigate2,'eerste navigatie')
information(masternavigate,'homepage')
print(masterlogin)




print("dit is hoeveel % van de users een login tijd hebben die korter is dan 5 seconden",masterlogin.index(5.0)/len(masterlogin)*100)
print("dit is hoeveel % van de users een plugin tijd hebben die korter is dan 5 seconden",masterplugin.index(5.0)/len(masterplugin)*100)
#print("dit zijn de statestieken van navigatie",masternavigate2)

#print(masternavigate)
#print(masterlogin)
#print(ninty)


