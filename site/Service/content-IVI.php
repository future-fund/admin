<?php

    include (__DIR__ . '/../Repository/IVIRepositoryInterface.php');
    include 'db_connection.php';
    $conn = OpenCon();
    $sql = "SELECT Id, Name FROM `Investment_option`";
    $result = $conn->query($sql);

function debug_to_console($data) {
    $output = $data;
    if (is_array($output))
        $output = implode(',', $output);

    echo "<script>alert('" . $output . "');</script>";
}
?>
    <form action="#"  method="POST">
        <div class="fishadd__box--wrapper">
    <?php
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) { ?>
                <div class="box">
                    <h2 class="investmentOptionName"><?php echo $row ['Name']?></h2>
                    <div class="fishadd__bottom">
                        <label for="thing" style='float:left; margin-left: 13%'>Cash amount:</label>
                        <input name="cash<?php echo $row['Id'] ?>" type="number" step=".01" style='width: 45%;' required> 
                        <br>
                        <br>
                        <label for="thing" style='float:left; margin-left: 13%'>Invested amount:</label>
                        <input name="invested<?php echo $row['Id'] ?>" type="number" step=".01" style='width: 45%;' required> 
                        <br>
                        <br>
                        <label for="thing" style='float:left; margin-left: 13%'>when:</label>
                        <input name="transfer_datetime<?php echo $row['Id'] ?>" type="datetime-local" value=<?php echo date("Y-m-d h:i") ?>>
                        <br>
                        <br>
                    </div>
                </div>
            <?php  
                }
            }else{
                echo "0 funds found";
            }
            $conn->close();
            ?>
    </div>
    <input type="submit" name="Submit" value="save" >
</form>

<style>
    .box{
        width: 40%;
        background-color: #90EE90;
        position: relative;
        border: 2px solid black;
        border-radius: 10%;
        margin: 10px;
    }
    .box input{
        width: 70%;
    }
</style>

<?php


function save() {
    $ids = getInvestmentOptionIds_IVI_Repository();

    foreach ($ids as &$recordID) {
        $cash = $_POST["cash$recordID"];
        $datetime = $_POST["transfer_datetime$recordID"];
        $invested = $_POST["invested$recordID"];
        $idealVal = calcIdealVal_IVI_Repository($recordID, $invested, $cash);

        saveInvestmentOptionValuation_IVI_Repository($recordID, $datetime, $invested, $cash, $idealVal);
    }
}
if(isset($_POST['Submit'])){
    save();
    header( 'Location: /nl/money-per-investment-method' );
}
?>