<form action="#"  method="POST">
    <div class="fishadd__box--wrapper">
        <table style="width:60%" id="results">
            <tr>
                <th>Test</th>
                <th>Correct</th>
            </tr>
        </table>
    </div>
    <input type="submit" name="Submit" value="save" >
</form>



<?php
include 'db_connection_test.php';
include (__DIR__ . '/../Repository/DGPIMFormRepositoryInterface.php');
include (__DIR__ . '/../Repository/TP_DGPIMFormRepositoryInterface.php');

    if(isset($_POST['Submit'])){
        testPageDGPIMForm();
    }

    function testPageDGPIMForm(){
        clearDatabase();

        // test 4.3.1
        addRow("'test 4.3.1'", "'calcAmount'");
        test4_3_1();
        clearDatabase();

        //testing 4.3.2
        addRow("'test 4.3.1'", "'processForm'");
        test4_3_2();
    }

    function test4_3_1(){
        //create iov for testing (good year) 2000 - 1900 = 100, 1% = 19
        createInvestment_option_valuation(1900.00, 100.00, 1900.00);
        //check
        $cel1 = "'Table: Investment_option_valuation <br> functie: calc_Amount (good year)'";
        (calcAmount(1) ==  100) ? $cel2 = "'✔'" : $cel2 = "'X'";
        addRow($cel1, $cel2);

        clearDatabase();

        //create iov for testing (bad year)
        createInvestment_option_valuation(1900.00, 100.00, 1990.00);
        //check
        $cel1 = "'Table: Investment_option_valuation <br> functie: calc_Amount (bad year)'";
        (calcAmount(1) ==  20) ? $cel2 = "'✔'" : $cel2 = "'X'";
        addRow($cel1, $cel2);
    }

    function test4_3_2(){
        clearDatabase();
        prepareDBFor4_3_2();

        //simulate and test good year
        //investment_option_valuation (would normally be created at page 4.2)
        $conn = OpenCon();
        $sqlInsertGoodYear = $conn->prepare("INSERT INTO `Investment_option_valuation` (`Id`, `Investment_option_Id`, `Transferred_Id`, `refresh_datetime`, `Invested_amount`, `cash_amount`, `ideal_valuation`, `for_calculation`) VALUES ('2', '1', NULL, '2021-01-12 16:48:41', '27', '2', 24.5, '1')");
        $sqlInsertGoodYear->execute();
        $conn->close();

        //act
        processForm(4.5, 4.5, 22.5, 27);

        //checking Transferred
        $jsonobj = array(
            'ideal_valuation' => 24.50
        );
        check_Transferred(json_encode($jsonobj));

        //checking Valuation_contribution
        check_Valuation_contribution(4);

        //checking Transferred_charity_Transferred
        check_Transferred_charity_Transferred(1);

        //checking Transferred_charity_Investment_distribution
        check_Transferred_charity_Investment_distribution(2, 0.5);

        //checking Transferred_charity
        $jsonobj = array(
            'transfer_amount' => 4.50,
            'transfer_datetime' => null,
            'status' => 0,
            'wppx_posts_Id' => 1
        );
        check_Transferred_charity(json_encode($jsonobj));

        //check if max row contains investment_option 1, Transferred_Id != null, invested_amount = 22.50, cash_amount = 2.00, ideal_valuation = 24.50, for_calculation = 0
        $jsonobj = array(
            'investment_option' => 1,
            'Transferred_Id' => null,
            'invested_amount' => 22.50,
            'cash_amount' => 2.00,
            'ideal_valuation' => 24.50,
            'for_calculation' => 0,
        );
        check_Investment_option_valuation(json_encode($jsonobj));
    }

function check_Transferred($expectedValuesJson)
{
    $expectedValuesJson = json_decode($expectedValuesJson);
    $transferred = json_decode(TP_getTransferred_DGPIMForm_Repository());

    $cel1 = "'Table: Transferred <br> Attribute: ideal_valuation'";
    ($expectedValuesJson->ideal_valuation ==  $transferred->ideal_valuation) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);
}

function check_Valuation_contribution($rowAmount)
{
    //check if contains 4 rows (2 made in setup, 2 cloned in actual function)
    $Valuation_contribution_Rows = TP_Valuation_contribution_Rows_DGPIMForm_Repository();
    $actualRows = null;

    if ($Valuation_contribution_Rows->num_rows > 0) {
        while($rowVal = $Valuation_contribution_Rows->fetch_assoc()) {
            $actualRows = $rowVal['amount'];
        }
    }

    $cel1 = "'Table: Valuation_contribution <br> Rows: $rowAmount'";
    ($rowAmount ==  $actualRows) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);
}

function check_Transferred_charity_Transferred($rowAmount)
{
    // check if contains 1 row
    $Transferred_charity_Transferred_Rows = TP_Transferred_charity_Transferred_Rows_DGPIMForm_Repository();
    $actualRows = null;

    if ($Transferred_charity_Transferred_Rows->num_rows > 0) {
        while($rowVal = $Transferred_charity_Transferred_Rows->fetch_assoc()) {
            $actualRows = $rowVal['amount'];
        }
    }

    $cel1 = "'Table: Transferred_charity_Transferred <br> Rows: $rowAmount'";
    ($rowAmount ==  $actualRows) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);
}

function check_Transferred_charity_Investment_distribution($expectedRows, $expectedFraction_distribution_money_amount)
{
    //check if contains 2 rows with both 0.5 in 'fraction_distribution_money_amount'
    $Transferred_charity_Transferred_Rows = TP_Transferred_charity_Investment_distribution_DGPIMForm_Repository();
    $actualFraction_distribution_money_amount = null;
    $rowCount = null;

    if ($Transferred_charity_Transferred_Rows->num_rows > 0) {
        while($rowVal = $Transferred_charity_Transferred_Rows->fetch_assoc()) {
            $actualFraction_distribution_money_amount = $rowVal['fraction_distribution_money_amount'];
            $rowCount += 1;
        }
    }

    $cel1 = "'Table: Transferred_charity_Investment_distribution <br> Rows: $expectedRows'";
    ($expectedRows ==  $rowCount) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);

    $cel1 = "'Table: Transferred_charity_Investment_distribution <br> fraction_distribution_money_amount'";
    ($expectedFraction_distribution_money_amount ==  $actualFraction_distribution_money_amount) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);
}

function check_Transferred_charity($expectedValuesJson)
{
    //check if contains row with transfer_amount = 4.50, transfer_datetime = null, status = 0 and wppx_posts_Id = 1
    $expectedValuesJson = json_decode($expectedValuesJson);
    $transferred_charity = json_decode(TP_Transferred_charity_DGPIMForm_Repository());

    $cel1 = "'Table: Transferred_charity <br> Attribute: transfer_amount'";
    ($expectedValuesJson->transfer_amount ==  $transferred_charity->transfer_amount) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);

    $cel1 = "'Table: Transferred_charity <br> Attribute: transfer_datetime'";
    ($expectedValuesJson->transfer_datetime ==  $transferred_charity->transfer_datetime) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);

    $cel1 = "'Table: Transferred_charity <br> Attribute: status'";
    ($expectedValuesJson->status ==  $transferred_charity->status) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);

    $cel1 = "'Table: Transferred_charity <br> Attribute: wppx_posts_Id'";
    ($expectedValuesJson->wppx_posts_Id ==  $transferred_charity->wppx_posts_Id) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);
}

function check_Investment_option_valuation($expectedValuesJson)
{
    //check if max row contains investment_option 1, Transferred_Id != null, invested_amount = 22.50, cash_amount = 2.00, ideal_valuation = 24.50, for_calculation = 0

    $expectedValuesJson = json_decode($expectedValuesJson);
    $investment_option_valuation = json_decode(TP_Investment_option_valuation_DGPIMForm_Repository());

    $cel1 = "'Table: Investment_option_valuation <br> Attribute: investment_option'";
    ($expectedValuesJson->investment_option ==  $investment_option_valuation->investment_option) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);

    $cel1 = "'Table: Investment_option_valuation <br> Attribute: Transferred_Id'";
    ($expectedValuesJson->Transferred_Id !=  $investment_option_valuation->Transferred_Id) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);

    $cel1 = "'Table: Investment_option_valuation <br> Attribute: invested_amount'";
    ($expectedValuesJson->invested_amount ==  $investment_option_valuation->invested_amount) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);

    $cel1 = "'Table: Investment_option_valuation <br> Attribute: cash_amount'";
    ($expectedValuesJson->cash_amount ==  $investment_option_valuation->cash_amount) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);

    $cel1 = "'Table: Investment_option_valuation <br> Attribute: ideal_valuation'";
    ($expectedValuesJson->ideal_valuation ==  $investment_option_valuation->ideal_valuation) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);

    $cel1 = "'Table: Investment_option_valuation <br> Attribute: for_calculation'";
    ($expectedValuesJson->for_calculation ==  $investment_option_valuation->for_calculation) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);
}

function createInvestment_option_valuation($investedAmount, $cashAmount, $idealValuation){
        echo $investedAmount, $idealValuation;
    $conn = OpenCon();
    $sqlInsertGoodYear = $conn->prepare("INSERT INTO `Investment_option_valuation` (`Id`, `Investment_option_Id`, `Transferred_Id`, `refresh_datetime`, `Invested_amount`, `cash_amount`, `ideal_valuation`, `for_calculation`) 
                                                VALUES ('1', '1', NULL, '2021-01-12 13:24:28', ?, ?, ?, '1')");
    $sqlInsertGoodYear->bind_param('ddd', $investedAmount, $cashAmount, $idealValuation);
    $sqlInsertGoodYear->execute();
    $conn->close();
}

function prepareDBFor4_3_2(){
    //wppx_posts
    $conn = OpenCon();
    $sqlInsertGoodYear = $conn->prepare("INSERT INTO `wppx_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES ('1', '1', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', '', 'myCharity', '', 'publish', 'open', 'open', '', 'charityName', '', '', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', '', '0', '', '0', 'campaign', '', '0');");
    $sqlInsertGoodYear->execute();
    $conn->close();

    //wppx_charitable_donors
    $conn = OpenCon();
    $sqlInsertGoodYear = $conn->prepare("INSERT INTO `wppx_charitable_donors` (`donor_id`, `user_id`, `email`, `first_name`, `last_name`, `date_joined`, `data_erased`, `contact_consent`) VALUES ('1', NULL, 'test@test', 'test', 'test', '2021-01-05 15:05:48', NULL, NULL)");
    $sqlInsertGoodYear->execute();
    $conn->close();
    //wppx_charitable_campaign_donations
    $conn = OpenCon();
    $sqlInsertGoodYear = $conn->prepare("INSERT INTO `wppx_charitable_campaign_donations` (`campaign_donation_id`, `donation_id`, `donor_id`, `campaign_id`, `campaign_name`, `amount`) VALUES ('1', '1', '1', '1', 'test', '10')");
    $sqlInsertGoodYear->execute();
    $conn->close();

    $conn = OpenCon();
    $sqlInsertGoodYear = $conn->prepare("INSERT INTO `wppx_charitable_campaign_donations` (`campaign_donation_id`, `donation_id`, `donor_id`, `campaign_id`, `campaign_name`, `amount`) VALUES ('2', '2', '1', '1', 'test', '10')");
    $sqlInsertGoodYear->execute();
    $conn->close();

    //investment_distribution
    $conn = OpenCon();
    $sqlInsertGoodYear = $conn->prepare("INSERT INTO `Investment_distribution` (`Id`, `WPPX_charitable_campaign_donations_Id`, `Investment_option_id`, `Transferred_Id`, `fraction_distribution_donation`) VALUES ('1', '1', '1', '', '1')");
    $sqlInsertGoodYear->execute();
    $conn->close();

    $conn = OpenCon();
    $sqlInsertGoodYear = $conn->prepare("INSERT INTO `Investment_distribution` (`Id`, `WPPX_charitable_campaign_donations_Id`, `Investment_option_id`, `Transferred_Id`, `fraction_distribution_donation`) VALUES ('2', '2', '1', '', '1')");
    $sqlInsertGoodYear->execute();
    $conn->close();

    //valuation_contribution
    $conn = OpenCon();
    $sqlInsertGoodYear = $conn->prepare("INSERT INTO `Valuation_contribution` (`Id`, `Investment_distribution_Id`, `Transferred_id`, `fraction_investment_option`) VALUES ('1', '1', '', '0.5')");
    $sqlInsertGoodYear->execute();
    $conn->close();

    $conn = OpenCon();
    $sqlInsertGoodYear = $conn->prepare("INSERT INTO `Valuation_contribution` (`Id`, `Investment_distribution_Id`, `Transferred_id`, `fraction_investment_option`) VALUES ('2', '2', '', '0.5')");
    $sqlInsertGoodYear->execute();
    $conn->close();

    //transferred
    $conn = OpenCon();
    $sqlInsertGoodYear = $conn->prepare("INSERT INTO `Transferred` (`Id`, `Investment_option_Id`, `datetime`, `calculated_transfer_amount`, `transfer_amount`, `old_cash_amount`, `new_cash_amount`, `new_invested_amount`, `old_invested_amount`, `ideal_valuation`, `status`, `transfer_Id`) VALUES ('1', '1', '2020-11-09 16:34:10', '0', '0', '0', '0', '0', '0', '0', 'out', 'created')");
    $sqlInsertGoodYear->execute();
    $conn->close();


    $conn = OpenCon();
    $sqlInsertGoodYear = $conn->prepare("INSERT INTO `Transferred` (`Id`, `Investment_option_Id`, `datetime`, `calculated_transfer_amount`, `transfer_amount`, `old_cash_amount`, `new_cash_amount`, `new_invested_amount`, `old_invested_amount`, `ideal_valuation`, `status`, `transfer_Id`) VALUES ('2', '1', '2020-12-31 16:34:10', '20', '20', '0', '2', '18', '0', '20', 'in', '2donaties')");
    $sqlInsertGoodYear->execute();
    $conn->close();

    //Update investment_distribution transferred_Id
    $conn = OpenCon();
    $sqlInsertGoodYear = $conn->prepare("UPDATE Investment_distribution SET Investment_distribution.Transferred_Id = 2;");
    $sqlInsertGoodYear->execute();
    $conn->close();

    //Update Valuation_contribution transferred_Id
    $conn = OpenCon();
    $sqlInsertGoodYear = $conn->prepare("UPDATE Valuation_contribution SET Valuation_contribution.Transferred_id = 2;");
    $sqlInsertGoodYear->execute();
    $conn->close();


    //investment_option_valuation
    $conn = OpenCon();
    $sqlInsertGoodYear = $conn->prepare("INSERT INTO `Investment_option_valuation` (`Id`, `Investment_option_Id`, `Transferred_Id`, `refresh_datetime`, `Invested_amount`, `cash_amount`, `ideal_valuation`, `for_calculation`) VALUES ('1', '1', '2', '', '18', '2', '20', '0')");
    $sqlInsertGoodYear->execute();
    $conn->close();
}


function clearDatabase(){
    $conn = OpenCon();

    $sql = $conn->prepare("DELETE FROM Transferred_charity_Investment_distribution;");
    $sql->execute();
    $sql = $conn->prepare("DELETE FROM Transferred_charity;");
    $sql->execute();
    $sql = $conn->prepare("DELETE FROM Transferred_charity_Transferred;");
    $sql->execute();
    $sql = $conn->prepare("DELETE FROM Investment_option_valuation;");
    $sql->execute();
    $sql = $conn->prepare("DELETE FROM Valuation_contribution;");
    $sql->execute();
    $sql = $conn->prepare("DELETE FROM Transferred;");
    $sql->execute();
    $sql = $conn->prepare("DELETE FROM wppx_posts;");
    $sql->execute();
    $sql = $conn->prepare("DELETE FROM wppx_charitable_campaign_donations;");
    $sql->execute();
    $sql = $conn->prepare("DELETE FROM wppx_charitable_donors;");
    $sql->execute();
    $sql = $conn->prepare("DELETE FROM Investment_distribution;");
    $sql->execute();

    $conn->close();
}

function addRow($cel1, $cel2){
    echo "<script> 
              var table = document.getElementById('results');
              var row = table.insertRow();
              var Test = row.insertCell();
              var Correct = row.insertCell();
              Test.innerHTML = $cel1;
              Correct.innerHTML = $cel2;
          </script>";
}

// =======================================================================================================================================================================
// =============================================================DGPIM code==================================================================================================
// =======================================================================================================================================================================

function calcAmount($fundId) {
    $calcAmount = getCalcAmount_DGIMPForm_Repository($fundId);
    $badConversion = getBadConversion_DGIMPForm_Repository($fundId);
    echo "$ calcAmount: $calcAmount <br>";
    echo "$ badConversion: $badConversion <br>";

    // check if %bad_conversion should be used
    $toSubtract = max($badConversion, $calcAmount);

    echo number_format((float) $toSubtract, 2, '.', '');
    return number_format((float) $toSubtract, 2, '.', '');
}


function processForm($calcAmount, $invested, $worthAfter, $worthBefore){
    $fundId = 1; //in the real code this is a list of funds

    TP_CreateTransferTable_DGIMPForm_Repository($fundId, $calcAmount, $invested, $worthAfter, $worthBefore); //CD

    createValuation_contribution_DGIMPForm_Repository($fundId); //B2

    TP_createInvestment_option_valuation_DGIMPForm_Repository($fundId, $worthAfter); //C2

    calculateCharityDonation(); //D0 D1
}

function calculateCharityDonation(){
    $charityDontations = array();
    echo "<br>";

    $valIds = array();

    $resultValCon = getValuation_contribution_DGIMPForm_Repository();

    if ($resultValCon->num_rows > 0) {
        while($rowValCon = $resultValCon->fetch_assoc()) {
            $valConId = $rowValCon['Id'];

            //get the charity + the amount that needs to go to this charity
            $charityId = charityByValConId_DGIMPForm_Repository($valConId);
            $amount = moneyToDonate_DGIMPForm_Repository($valConId);

            $valIds["$valConId"] = (float)$amount;

            echo "valConId:   .$valConId <br>";
            echo "amount:   .$amount <br>";
            echo "charityId:   .$charityId <br>";

            if(array_key_exists($charityId,$charityDontations)){
                $charityDontations["$charityId"] = (float)$charityDontations["$charityId"] + (float)$amount ;
            }else{
                $charityDontations["$charityId"] = (float)$amount;
            }
        }
        echo "<br>";
        echo "<br>";
        foreach ($charityDontations as $charityIdKey => $amountKey) {

            $Transferred_Charity_Id = createTransferred_charity_DGIMPForm_Repository($charityIdKey, $amountKey); // D1

            foreach ($valIds as $valConId => $amountKeyVal) {
                $charityId = charityByValConId_DGIMPForm_Repository($valConId);
                echo "charityId:   .$charityId == .$charityIdKey <br>";
                echo "valConId:   .$valConId <br>";
                echo "amountKey:   .$amountKey <br>";
                echo "amountKeyVal:   .$amountKeyVal <br>";

                if($charityId == $charityIdKey){
                    $fractionAmountKey = $amountKeyVal / $amountKey;
                    $investment_distributionId = getInvestment_distributionId_DGIMPForm_Repository($valConId);

                    createTransferred_charity_Investment_distribution_DGIMPForm_Repository($Transferred_Charity_Id, $investment_distributionId, $fractionAmountKey); // D2
                }
            }
            $resultTransferredIdByCharity = getTransferredId_DGIMPForm_Repository($charityIdKey);

            if ($resultTransferredIdByCharity->num_rows > 0) {
                echo"THIS IS A TEST $Transferred_Charity_Id";
                while($rowTransferredId = $resultTransferredIdByCharity->fetch_assoc()) {
                    createTransferred_charity_Transferred_DGIMPForm_Repository($rowTransferredId, $Transferred_Charity_Id); //D0
                }
            }
        }
    }
}

?>