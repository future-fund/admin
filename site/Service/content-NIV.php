<?php
include 'db_connection.php';
include (__DIR__ . '/../Repository/NIVRepositoryInterface.php');

$sql = "select o.Id, o.Name, sum(cd.amount) as amount
            from Investment_option o join Investment_distribution d on o.Id = d.Investment_option_Id
            join wppx_charitable_campaign_donations cd on d.WPPX_charitable_campaign_donations_Id = cd.campaign_donation_id
            left join Valuation_contribution c on d.Id = c.Investment_distribution_Id
            where c.Id is null
            group by o.Id";

$conn = OpenCon();
$result = $conn->query($sql);

?>
<form action="#"  method="POST">
    <?php
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $id =  $row['Id'];
            $old_cash_amount = null;
            $sql1 = "select COALESCE(new_cash_amount, 0) as new_cash_amount
                    from Transferred
                    where Investment_option_Id = $id
                    and Id = (select MAX(Id) from Transferred where Investment_option_Id = $id)";
            $result1 = $conn->query($sql1);
            if ($result1->num_rows > 0) {
                while ($row1 = $result1->fetch_assoc()) {
                    $old_cash_amount = $row1['new_cash_amount'];
                }
            }
            if($old_cash_amount == null){
                $old_cash_amount = 0;
            }
            ?>
            <div id="<?php echo $row['Id']?>" style="border: 2px solid black; border-radius: 5px; width: 400px; float: left; text-align: left;">
                <h2><?php echo $row['Name']?></h2>
                <p>Oude cash waarde: <?php echo $old_cash_amount?></p>
                <p>Geld nieuwe donaties: <?php echo $row['amount'] + 0.00 ?></p>
                <p>Totaal te gebruiken: : <?php echo $row['amount'] + $old_cash_amount?></p>
                <br>
                <div>Old invested worth: <input name="oldInvestedWaarde<?php echo $row['Id'] ?>" type="number" step=".01" style='height: 35px; width: 25%;' required></div>
                <div>Nieuwe invested waarde: <input name="nieuweInvestedWaarde<?php echo $row['Id'] ?>"  type="number" step=".01" style='height: 35px; width: 25%;' required></div>
                <div>Nieuwe cash waarde: <input name="nieuweCashWaarde<?php echo $row['Id'] ?>" type="number" step=".01" style='height: 35px; width: 25%;' required></div>
                <div>Transfer ID: <input name="transferId<?php echo $row['Id'] ?>" style='height: 35px; width: 75%;' required></div>
                <div>When: <input name="transfer_datetime<?php echo $row['Id'] ?>" type="date" value=<?php echo date("Y-m-d") ?>></div>
            </div>
            <?php
        }
    } else{
        echo "0 funds found";
    }
    $conn->close();
    ?>
    <input type="submit" name="Submit" value="save" >
</form>


<?php
function start($transfers){
    foreach ($transfers as &$transferEncoded) {
        $transfer = json_decode($transferEncoded);

        $Investment_option_Id = $transfer->Investment_option_Id;
        $new_invested_amount = $transfer->new_invested_amount;
        $new_cash_amount = $transfer->new_cash_amount;
        $old_invested_amount = $transfer->old_invested_amount;
        $old_cash_amount = $transfer->old_cash_amount;
        $conversion_datetime = $transfer->conversion_datetime;
        $maxTransferredId = $transfer->maxTransferredId;

        insertTransferred_NIV_Repository($transfer);

        $transferredId = getnew_invested_amountTransferIdByInvestmentOption_NIV_Repository($Investment_option_Id);

        createNewInvestment_option_valuation_NIV_Repository($Investment_option_Id, $new_cash_amount, $conversion_datetime, $new_invested_amount, $transferredId);

        $oldTotalVal = $old_invested_amount +$old_cash_amount;
        $newTotalVal = $new_invested_amount + $new_cash_amount;

        updateValuationContribution($Investment_option_Id, $oldTotalVal, $newTotalVal, $transferredId, $maxTransferredId);
    }
}

function updateValuationContribution($Investment_option_Id, $oldTotalVal, $newTotalVal, $transferId, $maxTransferredId){
    //update oude donaties
    $old_contributions = getNewFractionPerOldDonation_NIV_Repository($maxTransferredId, $oldTotalVal, $newTotalVal);
    $contributions = getFractionNewDonations_NIV_Repository($Investment_option_Id, $oldTotalVal, $newTotalVal, $old_contributions);

    insertValuationContribution_NIV_Repository($contributions, $transferId);
}

if(isset($_POST['Submit'])){
    $transfers = getValues_NIV_Repository();
    start($transfers);
    header( 'Location: /' );
}
?>
