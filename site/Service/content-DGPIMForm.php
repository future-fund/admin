<?php

include 'db_connection.php';
include (__DIR__ . '/../Repository/DGPIMFormRepositoryInterface.php');


$conn = OpenCon();

$sqlInvOptions = "SELECT Investment_option.Id, Investment_option.name from Investment_option";
$resultInvOptions = $conn->query($sqlInvOptions);
?>

    <form action="" method="post">
        <div>
            <?php
            if ($resultInvOptions->num_rows > 0) {
                // output data of each row
                while($rowInvOptions = $resultInvOptions->fetch_assoc()) {
                    ?>
    
                    <div class="box">
                        <br>
                        <h2 class="investmentName"><?php echo $rowInvOptions['name']?></h2>
                        <div>
                            <label style='float:left; margin-left: 13%'>Transfer id:</label><br>
                            <input class="input" name = "<?php echo $rowInvOptions['Id'] ?>_transferid" type="text" required/> 
                            <br>
                            <br>
                            <label name = "<?php echo $rowInvOptions['Id'] ?>_calcAmount">amount to subtract: <?php calcAmount($rowInvOptions['Id']) ?> </label> 
                            <br>
                            <br>
                            <label style='float:left; margin-left: 13%'>actual withdrawn invested:</label><br>
                            <input class="input" name = "<?php echo $rowInvOptions['Id'] ?>_invested" type="text" required/>
                            <br>
                            <br>
                            <label for="thing" style='float:left; margin-left: 13%'>Investment worth at moment of money withdraw:</label><br>
                            <input class="input" name = "<?php echo $rowInvOptions['Id'] ?>_worthBefore" type="text" required/> 
                            <br>
                            <br>
                            <label for="thing" style='float:left; margin-left: 13%'>Investment worth at the moment after withdraw:</label><br>
                            <input class="input" name = "<?php echo $rowInvOptions['Id'] ?>_worthAfter" type="text" required/> 
                            <br>
                            <br>
                            <label for="thing" style='float:left; margin-left: 13%'>when:</label><br>
                            <input class="input" name="<?php echo $rowInvOptions['Id'] ?>_transfer_datetime" type="datetime-local" value=<?php echo date("Y-m-d h:i") ?>>
                            <br>
                            <br>
                        </div>
                    </div>
                    <?php
                }
            }else{
                echo "0 funds found";
            }
            $conn->close();
            ?>
        </div>
        <input type="submit" name="Submit"/>
    </form>

    <style>
        .box{
            background-color: #90EE90;
            position: static;
            border-radius: 20px;
            margin: 10px;
            width: 40%;
        }
        .box label{
            margin-top: 7px;
        }
        .input{
            position: relative;
            width: 20%;
            margin-left: 10%; 
        }
    </style>

<?php

function calcAmount($fundId) {
    $calcAmount = getCalcAmount_DGIMPForm_Repository($fundId);
    $badConversion = getBadConversion_DGIMPForm_Repository($fundId);

    // check if %bad_conversion should be used
    $toSubtract = max($badConversion, $calcAmount);
    
    echo number_format((float) $toSubtract, 2, '.', '');
    return number_format((float) $toSubtract, 2, '.', '');
}

function processForm(){
    $listOfFundIds = getListOfFundIds_DGIMPForm_Repository();

    foreach($listOfFundIds as &$fundId){
        createTransferTable_DGIMPForm_Repository($fundId); //CD

        createValuation_contribution_DGIMPForm_Repository($fundId); //B2

        createInvestment_option_valuation_DGIMPForm_Repository($fundId); //C2
    }
    calculateCharityDonation(); //D0 D1
}

function calculateCharityDonation(){
    $charityDontations = array();
    echo "<br>";

    $valIds = array();

    $resultValCon = getValuation_contribution_DGIMPForm_Repository();

    if ($resultValCon->num_rows > 0) {
        while($rowValCon = $resultValCon->fetch_assoc()) {
            $valConId = $rowValCon['Id'];

            //get the charity + the amount that needs to go to this charity
            $charityId = charityByValConId_DGIMPForm_Repository($valConId);
            $amount = moneyToDonate_DGIMPForm_Repository($valConId);

            $valIds["$valConId"] = (float)$amount;

            echo "valConId:   .$valConId <br>";
            echo "amount:   .$amount <br>";
            echo "charityId:   .$charityId <br>";

            if(array_key_exists($charityId,$charityDontations)){
                $charityDontations["$charityId"] = (float)$charityDontations["$charityId"] + (float)$amount ;
            }else{
                $charityDontations["$charityId"] = (float)$amount;
            }
        }
        echo "<br>";
        echo "<br>";
        foreach ($charityDontations as $charityIdKey => $amountKey) {

            $Transferred_Charity_Id = createTransferred_charity_DGIMPForm_Repository($charityIdKey, $amountKey); // D1

            $resultTransferredIdByCharity = getTransferredId_DGIMPForm_Repository($charityIdKey);

            foreach ($valIds as $valConId => $amountKeyVal) {
                $charityId = charityByValConId_DGIMPForm_Repository($valConId);
                echo "charityId:   .$charityId == .$charityIdKey <br>";
                echo "valConId:   .$valConId <br>";
                echo "amountKey:   .$amountKey <br>";
                echo "amountKeyVal:   .$amountKeyVal <br>";

                if($charityId == $charityIdKey){
                    $fractionAmountKey = $amountKeyVal / $amountKey;
                    $investment_distributionId = getInvestment_distributionId_DGIMPForm_Repository($valConId);

                    createTransferred_charity_Investment_distribution_DGIMPForm_Repository($Transferred_Charity_Id, $investment_distributionId, $fractionAmountKey); // D2
                }
            }

            if ($resultTransferredIdByCharity->num_rows > 0) {
                while($rowTransferredId = $resultTransferredIdByCharity->fetch_assoc()) {
                    createTransferred_charity_Transferred_DGIMPForm_Repository($rowTransferredId, $Transferred_Charity_Id); //D0
                }
            }
        }
    }
}

if(isset($_POST['Submit'])){
    processForm();
    header( 'Location: /noteren-overgemaakt-geld/' );
}
?>