<?php

include 'db_connection.php';
include (__DIR__ . '/../Repository/FSRepositoryInterface.php';


$conn = OpenCon();

$sqlInvOptions = "select * from Investment_option WHERE ord(STATUS) = 1";
$resultInvOptions = $conn->query($sqlInvOptions);
?>
    <form action="" method="post">
        <?php
        if ($resultInvOptions->num_rows > 0) {
            // output data of each row
            while($rowInvOptions = $resultInvOptions->fetch_assoc()) {
                ?>

                <div class="box" onclick="select(<?php echo $rowInvOptions['Id']?>)">
                    <h2 class="investmentName"><?php echo $rowInvOptions['name']?></h2>
                    <div>
                        <h4 style='float:left; margin-left: 13%'><?php echo $rowInvOptions['description_short'] ?></h4>
                        <br>
                        <label style='float:left; margin-left: 13%'><?php echo $rowInvOptions['description_long'] ?></label>
                        <br>
                        <br>
                    </div>
                </div>
            <?php
            }
        }else{
            echo "0 funds found";
        }
        $conn->close();
        ?>
        <label id="selected"></label>
        <input type="submit" name="Submit"/>
    </form>

    <style>
        .box{
            background-color: #90EE90;
            position: static;
            border-radius: 20px;
            margin: 10px;
            width: 40%;
        }
        .box:hover{
            background-color: #90eebd;
            width: 45%;
        }
    </style>
    <script>
        function select($fundId) {
            document.getElementById("selected").innerHTML = "selected fund: " + $fundId;
            document.cookie = "selectedFund="+ $fundId;
        }
    </script>

<?php

function save() {
    $donor_id = charitable_get_user( get_current_user_id() )->get_donor_id();
    $user_id = get_current_user_id();
    $fund_id = $_COOKIE["selectedFund"];
    echo $fund_id;
    echo $donor_id;
    echo $user_id;
    saveFundChoice_FS_Repository($donor_id, $fund_id);
}


if(isset($_POST['Submit'])){
    save();
}
?>