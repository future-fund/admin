<?php
    include 'db_connection.php';
    include (__DIR__ . '/../Repository/NOGRepositoryInterface.php');

    $conn = OpenCon();
    echo "Connected Successfully";


    $conn = OpenCon();

    $sqlOldNewDif = "SELECT t.Id, c.post_name, t.transfer_amount
                                    FROM  Transferred_charity t JOIN wppx_posts c ON t.WPPX_Posts_Id = c.Id
                                    WHERE t.status = 'not_donated'";

    $result = $conn->query($sqlOldNewDif);
    
?>
    <form action="#"  method="POST">
        <div class="fishadd__box--wrapper">
            <table style="width:100%">
                <tr>
                    <th>Charity</th>
                    <th>Date</th>
                    <th>Amount</th>
                    <th>Calculated amount</th>
                </tr>
                <?php
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                        ?>
                            <tr id="<?php echo $row['Id']?>">
                                <th><?php echo $row['post_name']?></th>
                                <th><input name="transfer_datetime<?php echo $row['Id'] ?>" type="date" value=<?php echo date("Y-m-d") ?>></th>
                                <th><input name="transfer_amount<?php echo $row['Id'] ?>" type="text" required></th>
                                <th><?php echo $row['transfer_amount']?></th>
                            </tr>
                        <?php
                        }
                    } else{
                        echo "0 funds found";
                    }
                    $conn->close();
                ?>
            </table>
        </div>
        <input type="submit" name="Submit" value="save" >
    </form>

<?php
    if(isset($_POST['Submit'])){
        save_NOG_Repository();
        header( 'Location:  /' );
    }
?>