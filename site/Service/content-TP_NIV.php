<form action="#"  method="POST">
    <div class="fishadd__box--wrapper">
        <table style="width:60%" id="results">
            <tr>
                <th>Test</th>
                <th>Correct</th>
            </tr>
        </table>
    </div>
    <input type="submit" name="Submit" value="save" >
</form>

<?php
include 'db_connection_test.php';
include (__DIR__ . '/../Repository/TP_NIVRepositoryInterface.php');
include (__DIR__ . '/../Repository/NIVRepositoryInterface.php');

if(isset($_POST['Submit'])){
    testPageNIV();
}

function testPageNIV(){
    clearDatabase_testDatabase();

    // test 5.2.1
    addRow("'<h2> test 5.2.1 </h2>'", "''");
    test5_2_1();

    // test 5.2.2
    addRow("'<h2> test 5.2.2 </h2>'", "''");
    test5_2_2();

    // test 5.2.3
    addRow("'<h2> test 5.2.3 </h2>'", "''");
    test5_2_3();

    // test 5.2.4
    addRow("'<h2> test 5.2.4 </h2>'", "''");
    test5_2_4();
}
function test5_2_1(){
    createTestDonation_testDatabase(40);
    createTestDonation_testDatabase(15);

    $new_invested_amount = 50;
    $new_cash_amount = 5;
    $old_invested_amount = 0;
    // act
    start(getValues_testDatabase($new_invested_amount, $new_cash_amount, $old_invested_amount));
    // Valuation_contribution
    check_fraction_investment_option();
    //Transferred

    $jsonobj = array(
        'calculatedAmount' => 55.00,
        'transfer_amount' => 50.00,
        'old_cash_amount' => 0.00,
        'new_cash_amount' => 5.00,
        'new_invested_amount' => 50.00,
        'old_invested_amount' => 0.00,
        'ideal_valuation' => 55.00
    );
    check_Transferred(json_encode($jsonobj));
    //Investment_option_valuation
    $jsonobj = array(
        'Invested_amount' => 50.00,
        'cash_amount' => 5.00,
        'ideal_valuation' => 55.00
    );
    check_Investment_option_valuation(json_encode($jsonobj));
}

function test5_2_2(){
    createTransferredOut_testDatabase(5, 50, 55);

    createTestDonation_testDatabase(10);

    $new_invested_amount = 60;
    $new_cash_amount = 5;
    $old_invested_amount = 50;
    // act
    start(getValues_testDatabase($new_invested_amount, $new_cash_amount, $old_invested_amount));
    // Valuation_contribution
    check_fraction_investment_option();
    //Transferred

    $jsonobj = array(
        'calculatedAmount' => 15.00,
        'transfer_amount' => 10.00,
        'old_cash_amount' => 5.00,
        'new_cash_amount' => 5.00,
        'new_invested_amount' => 60.00,
        'old_invested_amount' => 50.00,
        'ideal_valuation' => 65.00
    );
    check_Transferred(json_encode($jsonobj));
    //Investment_option_valuation
    $jsonobj = array(
        'Invested_amount' => 60.00,
        'cash_amount' => 5.00,
        'ideal_valuation' => 65.00
    );
    check_Investment_option_valuation(json_encode($jsonobj));
}
function test5_2_3(){
    createTransferredOut(5, 60, 65);

    createTestDonation_testDatabase(20);

    $new_invested_amount = 89;
    $new_cash_amount = 2;
    $old_invested_amount = 66;
    // act
    start(getValues_testDatabase($new_invested_amount, $new_cash_amount, $old_invested_amount));
    // Valuation_contribution
    check_fraction_investment_option();
    //Transferred
    $jsonobj = array(
        'calculatedAmount' => 25.00,
        'transfer_amount' => 23.00,
        'old_cash_amount' => 5.00,
        'new_cash_amount' => 2.00,
        'new_invested_amount' => 89.00,
        'old_invested_amount' => 66.00,
        'ideal_valuation' => 83.00
    );

    check_Transferred(json_encode($jsonobj));
    //Investment_option_valuation
    $jsonobj = array(
        'Invested_amount' => 89.00,
        'cash_amount' => 2.00,
        'ideal_valuation' => 83.00
    );
    check_Investment_option_valuation(json_encode($jsonobj));
}

function test5_2_4(){
    createTransferredOut(2.00, 89.00, 86.00);

    createTestDonation_testDatabase(5);

    $new_invested_amount = 87;
    $new_cash_amount = 0.10;
    $old_invested_amount = 80.10;
    // act
    start(getValues_testDatabase($new_invested_amount, $new_cash_amount, $old_invested_amount));
    // Valuation_contribution
    check_fraction_investment_option();
    //Transferred
    $jsonobj = array(
        'calculatedAmount' => 7.00,
        'transfer_amount' => 6.90,
        'old_cash_amount' => 2.00,
        'new_cash_amount' => 0.10,
        'new_invested_amount' => 87.00,
        'old_invested_amount' => 80.10,
        'ideal_valuation' => 86.79
    );
    check_Transferred(json_encode($jsonobj));
    //Investment_option_valuation
    $jsonobj = array(
        'Invested_amount' => 87.00,
        'cash_amount' => 0.10,
        'ideal_valuation' => 86.79
    );
    check_Investment_option_valuation(json_encode($jsonobj));
}


function check_Transferred($expectedValuesJson)
{
    $expectedValuesJson = json_decode($expectedValuesJson);
    $transfer = json_decode(getLastTransfer_testDatabase());

    $cel1 = "'Table: Transferred <br> Attribute: calculated_transfer_amount'";
    ($expectedValuesJson->calculatedAmount ==  $transfer->calculatedAmount) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);

    $cel1 = "'Table: Transferred <br> Attribute: transfer_amount'";
    ($expectedValuesJson->transfer_amount ==  $transfer->transfer_amount) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);

    $cel1 = "'Table: Transferred <br> Attribute: old_cash_amount'";
    ($expectedValuesJson->old_cash_amount ==  $transfer->old_cash_amount) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);

    $cel1 = "'Table: Transferred <br> Attribute: new_cash_amount'";
    ($expectedValuesJson->new_cash_amount ==  $transfer->new_cash_amount) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);

    $cel1 = "'Table: Transferred <br> Attribute: new_invested_amount'";
    ($expectedValuesJson->new_invested_amount ==  $transfer->new_invested_amount) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);

    $cel1 = "'Table: Transferred <br> Attribute: old_invested_amount'";
    ($expectedValuesJson->old_invested_amount ==  $transfer->old_invested_amount) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);

    $cel1 = "'Table: Transferred <br> Attribute: ideal_valuation'";
    ($expectedValuesJson->ideal_valuation ==  $transfer->ideal_valuation) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);
}

function check_Investment_option_valuation($expectedValuesJson){
    $expectedValuesJson = json_decode($expectedValuesJson);
    $Investment_option_valuation = json_decode(getInvestment_option_valuation_testDatabase());

    $cel1 = "'Table: Investment_option_valuation <br> Attribute: Invested_amount'";
    ($expectedValuesJson->Invested_amount ==  $Investment_option_valuation->Invested_amount) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);

    $cel1 = "'Table: Investment_option_valuation <br> Attribute: cash_amount'";
    ($expectedValuesJson->cash_amount ==  $Investment_option_valuation->cash_amount) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);

    $cel1 = "'Table: Investment_option_valuation <br> Attribute: ideal_valuation'";
    ($expectedValuesJson->ideal_valuation ==  $Investment_option_valuation->ideal_valuation) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);
}

function check_fraction_investment_option(){
    $total_fraction_transfer = getTotalTransferFraction_testDatabase();
    $cel1 = "'Table: Valuation_contribution <br> Attribute: fraction_investment_option wordt 1'";
    (strlen($total_fraction_transfer) == 1) ? $cel2 = "'✔'" : $cel2 = "'X'";
    addRow($cel1, $cel2);
}

function addRow($cel1, $cel2){
    echo "<script> 
              var table = document.getElementById('results');
              var row = table.insertRow();
              var Test = row.insertCell();
              var Correct = row.insertCell();
              Test.innerHTML = $cel1;
              Correct.innerHTML = $cel2;
          </script>";
}
// =======================================================================================================================================================================
// =============================================================NIV code==================================================================================================
// =======================================================================================================================================================================

function start($transfers){
    foreach ($transfers as &$transferEncoded) {
        $transfer = json_decode($transferEncoded);

        $Investment_option_Id = $transfer->Investment_option_Id;
        $new_invested_amount = $transfer->new_invested_amount;
        $new_cash_amount = $transfer->new_cash_amount;
        $old_invested_amount = $transfer->old_invested_amount;
        $old_cash_amount = $transfer->old_cash_amount;
        $conversion_datetime = $transfer->conversion_datetime;
        $maxTransferredId = $transfer->maxTransferredId;

        insertTransferred_NIV_Repository($transfer);

        $transferredId = getnew_invested_amountTransferIdByInvestmentOption_NIV_Repository($Investment_option_Id);

        createNewInvestment_option_valuation_NIV_Repository($Investment_option_Id, $new_cash_amount, $conversion_datetime, $new_invested_amount, $transferredId);

        $oldTotalVal = $old_invested_amount +$old_cash_amount;
        $newTotalVal = $new_invested_amount + $new_cash_amount;

        updateValuationContribution($Investment_option_Id, $oldTotalVal, $newTotalVal, $transferredId, $maxTransferredId);
    }
}

function updateValuationContribution($Investment_option_Id, $oldTotalVal, $newTotalVal, $transferId, $maxTransferredId){
    //update oude donaties
    $old_contributions = getNewFractionPerOldDonation_NIV_Repository($maxTransferredId, $oldTotalVal, $newTotalVal);
    $contributions = getFractionNewDonations_NIV_Repository($Investment_option_Id, $oldTotalVal, $newTotalVal, $old_contributions);

    insertValuationContribution_NIV_Repository($contributions, $transferId);
}
?>