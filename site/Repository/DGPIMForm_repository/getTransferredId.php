<?php
function getTransferredId($charityIdKey){
    $connTransferredIdByCharity = OpenCon();

    //Select the Transferred Id's of the latest conversionday who have a donation with the given charity
    $sqlTransferredIdByCharity = ("SELECT distinct Transferred.Id from Transferred 
                                    join Valuation_contribution on Valuation_contribution.Transferred_id = Transferred.Id 
                                    join Investment_distribution on Investment_distribution.Id = Valuation_contribution.Investment_distribution_Id 
                                    join wppx_charitable_campaign_donations on wppx_charitable_campaign_donations.campaign_donation_id = Investment_distribution.WPPX_charitable_campaign_donations_Id 
                                   where wppx_charitable_campaign_donations.campaign_id = $charityIdKey && Transferred.status = 'out' && Transferred.datetime = (select Max(Transferred.datetime) from Transferred)");

    $resultTransferredIdByCharity = $connTransferredIdByCharity->query($sqlTransferredIdByCharity);
    $connTransferredIdByCharity->close();
    return $resultTransferredIdByCharity;
}
?>