<?php
function getInvestment_distributionId($valConId){
    $conninvestment_distributionId = OpenCon();
    // calculates the amount of a %bad_conversion/'troostbedrag' this is 1% of the worth of the fund
    $sqlinvestment_distributionId = $conninvestment_distributionId->prepare("select Investment_distribution_Id from Valuation_contribution where Id = ?;");
    $sqlinvestment_distributionId->bind_param('i', $valConId);
    $sqlinvestment_distributionId->execute();
    $sqlinvestment_distributionId->bind_result($investment_distributionId);
    $sqlinvestment_distributionId->fetch();
    $conninvestment_distributionId->close();
    return $investment_distributionId;
}
?>
