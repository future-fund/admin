<?php
function createValuation_contribution($fundId){
    //sql query to clone the B2 rows of the last inward Transferred and couple this to the new Transferred row
    $connInsertContribution = OpenCon();
    $sqlInsertContribution = $connInsertContribution->prepare("INSERT INTO Valuation_contribution (Investment_distribution_Id, fraction_investment_option, Transferred_id) 
                                                                    select Valuation_contribution.Investment_distribution_Id, 
                                                                    Valuation_contribution.fraction_investment_option, 
                                                                    (SELECT Transferred.Id 
                                                                    FROM Transferred
                                                                    where Transferred.Investment_option_Id = ? and Transferred.status = 'out' 
                                                                    ORDER BY Transferred.datetime DESC
                                                                    LIMIT 1)
                                                                    from Valuation_contribution
                                                                    where Valuation_contribution.Transferred_id =  (SELECT Transferred.Id 
                                                                                                                    FROM Transferred
                                                                                                                    where Transferred.Investment_option_Id = ? and Transferred.status = 'in' 
                                                                                                                    ORDER BY Transferred.datetime DESC
                                                                                                                    LIMIT 1)
                                                                    ");
    $sqlInsertContribution->bind_param('ii', $fundId, $fundId);
    $sqlInsertContribution->execute();
    $connInsertContribution->close();
}
?>
