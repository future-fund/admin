<?php
function createTransferTable($fundId){
    $connInsertTransferred = OpenCon();
    //sql query to create a new Transferred row
    $sqlInsertTransferred = $connInsertTransferred->prepare("INSERT INTO `Transferred` (`Investment_option_Id`, `datetime`, `calculated_transfer_amount`, `transfer_amount`, `old_cash_amount`, `new_cash_amount`, `new_invested_amount`, `old_invested_amount`, `status`, `ideal_valuation`, `transfer_Id`) 
            VALUES (?, ?, ?, ?,                
                (SELECT Investment_option_valuation.cash_amount 
                FROM Investment_option_valuation
                where Investment_option_valuation.Investment_option_Id = ? 
                ORDER BY Investment_option_valuation.refresh_datetime DESC
                LIMIT 1)
                , (SELECT Investment_option_valuation.cash_amount 
                FROM Investment_option_valuation 
                where Investment_option_valuation.Investment_option_Id = ? 
                ORDER BY Investment_option_valuation.refresh_datetime DESC
                LIMIT 1), ?, ?, 'out', ?, ?)");


    $transCalcAmount = (float)calcAmount($fundId);
    $transInvested = (float)$_POST[$fundId . "_invested"];
    $transWorthAfter = (float)$_POST[$fundId . "_worthAfter"];
    $transWorthBefore = (float)$_POST[$fundId . "_worthBefore"];
    $datetime = $_POST[$fundId . "_transfer_datetime"];
    $transferId = $_POST[$fundId . "_transferid"];
    $transIdealVal = calcIdealVal($fundId, $transWorthBefore);

    $sqlInsertTransferred->bind_param('isddiiddds',
        $fundId,
        $datetime,
        $transCalcAmount,
        $transInvested,
        $fundId,
        $fundId,
        $transWorthAfter,
        $transWorthBefore,
        $transIdealVal,
        $transferId
    );
    $sqlInsertTransferred->execute();
    $connInsertTransferred->close();
}
?>
