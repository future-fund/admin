<?php
function getValuation_contribution(){
    $connValuationContributions = OpenCon();
    //select all the b2 rows connected to a 'transferred' CD of the latest conversionday
    $sqlValuationContributions = (" select * from Valuation_contribution 
                                    where Valuation_contribution.Transferred_id in ( 
                                        select Transferred.Id from Transferred where Transferred.datetime = ( select max(trans.datetime) from Transferred as trans ) 
                                        && Transferred.status = 'out'
                                    )");

    $resultValCon = $connValuationContributions->query($sqlValuationContributions);
    $connValuationContributions->close();
    return $resultValCon;
}
?>
