<?php
function charityByValConId($valConId){
    $connCharityId = OpenCon();
    //Houd nog geen rekenening met donatie waarbij meerdere goede doelen zijn geselecteerd
    $sqlCharityId = $connCharityId->prepare("SELECT wppx_charitable_campaign_donations.campaign_id FROM wppx_charitable_campaign_donations
	join Investment_distribution ON Investment_distribution.WPPX_charitable_campaign_donations_Id = wppx_charitable_campaign_donations.campaign_donation_id
    JOIN Valuation_contribution ON Valuation_contribution.Investment_distribution_Id = Investment_distribution.Id
    WHERE Valuation_contribution.Id = ?");

    $sqlCharityId->bind_param('i', $valConId);
    $sqlCharityId->execute();
    $sqlCharityId->bind_result($charityId);
    while ($sqlCharityId->fetch()) {
        $ActcharityId  = $charityId;
    }

    $connCharityId->close();

    return $charityId;
}
?>
