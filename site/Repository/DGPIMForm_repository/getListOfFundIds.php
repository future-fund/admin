<?php
function getListOfFundIds(){
    //get list of investment option id's
    $connFunds = OpenCon();
    $sql = "SELECT Id FROM Investment_option";
    $result = $connFunds->query($sql);
    $connFunds->close();
    if ($result->num_rows > 0) {
        $listOfFundIds=array();

        while($row = $result->fetch_assoc()) {
            array_push($listOfFundIds, (int) $row['Id']);
        }
    }
    return $listOfFundIds;
}
?>
