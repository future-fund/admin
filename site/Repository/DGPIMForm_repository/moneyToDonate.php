<?php
function moneyToDonate($valConId){
    $connMoney = OpenCon();

    $sqlMoney  = $connMoney->prepare("select (Valuation_contribution.fraction_investment_option * Transferred.transfer_amount) as amount 
                                    from Valuation_contribution join Transferred on Valuation_contribution.Transferred_id = Transferred.Id 
                                    where Valuation_contribution.Id = ?");

    $sqlMoney->bind_param('i', $valConId);
    $sqlMoney->execute();
    $sqlMoney->bind_result($amount);
    while ($sqlMoney->fetch()) {
        $monAmount = (float)$amount;
    }

    $connMoney->close();

    return (float)$monAmount;
}
?>
