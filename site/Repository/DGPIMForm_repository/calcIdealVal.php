<?php
function calcIdealVal($fundId, $transWorthBefore){
    $connCalcIdealVal = OpenCon();

    $sqlCalcIdealVal = $connCalcIdealVal->prepare("
        SELECT COALESCE(((? + new_cash_amount) / (new_invested_amount + new_cash_amount)
                * ideal_valuation - ideal_valuation)
            * (1-fraction_to_reinvest) + ideal_valuation,0) as amount
        FROM Transferred t
        JOIN Investment_option o on t.Investment_option_id = o.Id
        WHERE t.Investment_option_Id = ? and t.status = 'in' 
        ORDER BY t.datetime DESC, t.Id DESC
        LIMIT 1;");
    $sqlCalcIdealVal->bind_param('di', $transWorthBefore, $fundId);


    $sqlCalcIdealVal->execute();
    $sqlCalcIdealVal->bind_result($calcIdealVal);
    $sqlCalcIdealVal->fetch();
    $connCalcIdealVal->close();
    return $calcIdealVal;
}
?>
