<?php
function getBadConversion($fundId){
    $connBadConversion = OpenCon();
    // calculates the amount of a %bad_conversion/'troostbedrag' this is 1% of the worth of the fund
    $sqlBadConversion = $connBadConversion->prepare("select ((Investment_option_valuation.Invested_amount + Investment_option_valuation.cash_amount)  * 0.01) as badConversion from Investment_option_valuation
                                                    where Investment_option_valuation.Investment_option_Id = ?
                                                    ORDER BY Investment_option_valuation.refresh_datetime desc
                                                    LIMIT 1;");
    $sqlBadConversion->bind_param('i', $fundId);
    $sqlBadConversion->execute();
    $sqlBadConversion->bind_result($badConversion);
    $sqlBadConversion->fetch();
    $connBadConversion->close();
    return $badConversion;
}
?>
