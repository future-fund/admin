<?php
function createTransferred_charity($charityIdKey, $amountKey){
    $connInsertTransferred_charity = OpenCon();

    $sqlInsertTransferred_charity = $connInsertTransferred_charity->prepare("INSERT INTO `Transferred_charity` (`WPPX_Posts_Id`, `transfer_amount`, `status`) 
                                                                            VALUES (?, ?, 'not_donated')");
    $sqlInsertTransferred_charity->bind_param('id', $charityIdKey, $amountKey);
    $sqlInsertTransferred_charity->execute();

    $Transferred_Charity_Id = $sqlInsertTransferred_charity->insert_id;
    $connInsertTransferred_charity->close();
    return $Transferred_Charity_Id;
}
?>
