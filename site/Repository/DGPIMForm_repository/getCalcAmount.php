<?php
function getCalcAmount($fundId){
    $connCalcAmount = OpenCon();

    // calculates C2(invested) - C2(idealVal)
    $sqlOldNewDif = $connCalcAmount->prepare("SELECT(Investment_option_valuation.Invested_amount  + Investment_option_valuation.cash_amount - 
                                                (
                                                select OldIov.ideal_valuation 
                                                FROM Investment_option_valuation as OldIov
                                                where OldIov.Investment_option_Id = ? and OldIov.for_calculation = true
                                                ORDER BY OldIov.refresh_datetime DESC
                                                LIMIT 1)) as amount
                                            FROM Investment_option_valuation
                                            where Investment_option_valuation.Investment_option_Id = ? and Investment_option_valuation.for_calculation = true
                                            ORDER BY Investment_option_valuation.refresh_datetime DESC
                                            LIMIT 1;");
    $sqlOldNewDif->bind_param('ii',$fundId , $fundId);


    $sqlOldNewDif->execute();
    $sqlOldNewDif->bind_result($calcAmount);
    $sqlOldNewDif->fetch();
    $connCalcAmount->close();
    return $calcAmount;
}
?>
