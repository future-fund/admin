<?php
include 'DGPIMForm_repository/calcIdealVal.php';
    function calcIdealVal_DGIMPForm_Repository($fundId, $transWorthBefore){
        return calcIdealVal($fundId, $transWorthBefore);
    }

include 'DGPIMForm_repository/charityByValConId.php';
function charityByValConId_DGIMPForm_Repository($valConId){
    return charityByValConId($valConId);
}

include 'DGPIMForm_repository/createInvestment_option_valuation.php';
function createInvestment_option_valuation_DGIMPForm_Repository($fundId){
    createInvestment_option_valuation($fundId);
}

include 'DGPIMForm_repository/createTransferred_charity.php';
function createTransferred_charity_DGIMPForm_Repository($charityIdKey, $amountKey){
    return createTransferred_charity($charityIdKey, $amountKey);
}

include 'DGPIMForm_repository/createTransferred_charity_Investment_distribution.php';
function createTransferred_charity_Investment_distribution_DGIMPForm_Repository($Transferred_Charity_Id, $investment_distributionId, $fractionAmountKey){
    createTransferred_charity_Investment_distribution($Transferred_Charity_Id, $investment_distributionId, $fractionAmountKey);
}

include 'DGPIMForm_repository/createTransferred_charity_Transferred.php';
function createTransferred_charity_Transferred_DGIMPForm_Repository($rowTransferredId, $Transferred_Charity_Id){
    createTransferred_charity_Transferred($rowTransferredId, $Transferred_Charity_Id);
}

include 'DGPIMForm_repository/createTransferTable.php';
function createTransferTable_DGIMPForm_Repository($fundId){
    createTransferTable($fundId);
}

include 'DGPIMForm_repository/createValuation_contribution.php';
function createValuation_contribution_DGIMPForm_Repository($fundId){
    createValuation_contribution($fundId);
}

include 'DGPIMForm_repository/getBadConversion.php';
function getBadConversion_DGIMPForm_Repository($fundId){
    return getBadConversion($fundId);
}

include 'DGPIMForm_repository/getCalcAmount.php';
function getCalcAmount_DGIMPForm_Repository($fundId){
    return getCalcAmount($fundId);
}

include 'DGPIMForm_repository/getInvestment_distributionId.php';
function getInvestment_distributionId_DGIMPForm_Repository($valConId){
    return getInvestment_distributionId($valConId);
}

include 'DGPIMForm_repository/getListOfFundIds.php';
function getListOfFundIds_DGIMPForm_Repository(){
    return getListOfFundIds();
}

include 'DGPIMForm_repository/getTransferredId.php';
function getTransferredId_DGIMPForm_Repository($charityIdKey){
    return getTransferredId($charityIdKey);
}

include 'DGPIMForm_repository/getValuation_contribution.php';
function getValuation_contribution_DGIMPForm_Repository(){
    return getValuation_contribution();
}

include 'DGPIMForm_repository/moneyToDonate.php';
function moneyToDonate_DGIMPForm_Repository($valConId){
    return moneyToDonate($valConId);
}
?>
