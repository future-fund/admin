<?php
function save() {
    $conn = OpenCon();

    $sqlOldNewDif = "SELECT t.Id, c.post_name, t.transfer_amount
                FROM  Transferred_charity t JOIN wppx_posts c ON t.WPPX_Posts_Id = c.Id
                WHERE t.status = 'not_donated'";

    $result = $conn->query($sqlOldNewDif);

    if ($result->num_rows > 0) {
        $ids=array();

        while($row = $result->fetch_assoc()) {
            array_push($ids, $row['Id']);
        }

        foreach ($ids as &$recordID) {

            $transfer_datetime = $_POST["transfer_datetime$recordID"];
            $transfer_amount = $_POST["transfer_amount$recordID"];


            echo "<script>alert('$transfer_amount');</script>";
            echo "<script>alert('$transfer_datetime');</script>";

            $udpateTransferred_charity = $conn->prepare("UPDATE Transferred_charity 
                                                            SET transfer_amount = ?, transfer_datetime = ?, status = 'donated'
                                                            WHERE Id = ?");
            $udpateTransferred_charity->bind_param('dsi', $transfer_amount, $transfer_datetime, $recordID);
            $udpateTransferred_charity->execute();
        }
    } else {
        echo "<script>alert('No scheduled transfers were found in the system');</script>";
    }
    $conn->close();
}
?>