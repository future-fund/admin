<?php
include 'NIV_repository/insertValuationContribution.php';
function insertValuationContribution_NIV_Repository($transfers, $transferredId){
     insertValuationContribution($transfers, $transferredId);
}

include 'NIV_repository/getNewFractionPerOldDonation.php';
function getNewFractionPerOldDonation_NIV_Repository($maxTransferredId, $oldTotalVal, $newTotalVal){
    return getNewFractionPerOldDonation($maxTransferredId, $oldTotalVal, $newTotalVal);
}

include 'NIV_repository/getMaxTransferredId.php';
function getMaxTransferredId_NIV_Repository($Investment_option_Id){
    return getMaxTransferredId($Investment_option_Id);
}

include 'NIV_repository/getFractionNewDonations.php';
function getFractionNewDonations_NIV_Repository($Investment_option_Id, $oldTotalVal, $newTotalVal, $old_contributions){
    return getFractionNewDonations($Investment_option_Id, $oldTotalVal, $newTotalVal, $old_contributions);
}

include 'NIV_repository/createNewInvestment_option_valuation.php';
function createNewInvestment_option_valuation_NIV_Repository($Investment_option_Id, $cash_amount, $refresh_datetime, $Invested_amount, $Transferred_Id){
    createNewInvestment_option_valuation($Investment_option_Id, $cash_amount, $refresh_datetime, $Invested_amount, $Transferred_Id);
}

include 'NIV_repository/getnew_invested_amountTransferIdByInvestmentOption.php';
function getnew_invested_amountTransferIdByInvestmentOption_NIV_Repository($Investment_option_Id){
    return getnew_invested_amountTransferIdByInvestmentOption($Investment_option_Id);
}

include 'NIV_repository/insertTransferred.php';
function insertTransferred_NIV_Repository($transfer){
    insertTransferred($transfer);
}

include 'NIV_repository/getAllValues.php';
function getValues_NIV_Repository(){
    return getAllValues();
}
?>