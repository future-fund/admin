<?php
include 'TP_NIV_repository/getValues_tp.php';
function getValues_testDatabase($new_invested_amount, $new_cash_amount, $old_invested_amount){
    return getValues_tp($new_invested_amount, $new_cash_amount, $old_invested_amount);
}
include 'TP_NIV_repository/getTotalTransferFraction_tp.php';
function getTotalTransferFraction_testDatabase(){
    return getTotalTransferFraction_tp();
}

include 'TP_NIV_repository/createTestDonation_tp.php';
function createTestDonation_testDatabase($amount){
    createTestDonation($amount);
}

include 'TP_NIV_repository/deleteTestDonation_tp.php';
function deleteTestDonation_testDatabase(){
    deleteTestDonation();
}
include 'TP_NIV_repository/clearDatabase_tp.php';
function clearDatabase_testDatabase(){
    clearDatabase();
}
include 'TP_NIV_repository/getLastTransfer_tp.php';
function getLastTransfer_testDatabase(){
    return getLastTransfer();
}
include 'TP_NIV_repository/getInvestment_option_valuation_tp.php';
function getInvestment_option_valuation_testDatabase(){
    return getInvestment_option_valuation();
}

include 'TP_NIV_repository/createTransferredOut.php';
function createTransferredOut_testDatabase($old_cash_amount, $old_invested_amount, $ideal_valuation){
    return createTransferredOut($old_cash_amount, $old_invested_amount, $ideal_valuation);
}

?>