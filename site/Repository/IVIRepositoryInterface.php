<?php
include 'IVI_repository/getInvestmentOptionIds.php';
function getInvestmentOptionIds_IVI_Repository(){
    return getInvestmentOptionIds();
}

include 'IVI_repository/calcIdealVal.php';
function calcIdealVal_IVI_Repository($fundId, $transWorthBefore, $cash){
    return calcIdealVal($fundId, $transWorthBefore, $cash);
}

include 'IVI_repository/saveInvestmentOptionValuation.php';
function saveInvestmentOptionValuation_IVI_Repository($recordID, $datetime, $invested, $cash, $idealVal){
    saveInvestmentOptionValuation($recordID, $datetime, $invested, $cash, $idealVal);
}
?>