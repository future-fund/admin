<?php
function getInvestment_option_valuation(){
    $conn = OpenCon();
    $oldDonations = "SELECT * FROM Investment_option_valuation WHERE Id = (SELECT MAX(Id) FROM Investment_option_valuation)";
    $result = $conn->query($oldDonations);

    $jsonobj = null;
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $jsonobj = array(
                'Invested_amount' => $row['Invested_amount'],
                'cash_amount' => $row['cash_amount'],
                'ideal_valuation' => $row['ideal_valuation']
            );
        }
    }
    $conn->close();
    return json_encode($jsonobj);
}
?>