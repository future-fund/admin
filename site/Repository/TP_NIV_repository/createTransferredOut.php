<?php
function createTransferredOut($old_cash_amount, $old_invested_amount, $ideal_valuation)
{
    $conn = OpenCon();
// verhoudingen van vorige transfer overnemen naar nieuwe transfer
    $oldDonations = "SELECT * FROM Valuation_contribution WHERE Transferred_id = (SELECT MAX(Transferred_id) FROM Valuation_contribution)";
    $result = $conn->query($oldDonations);
    $transfers = array();

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $jsonobj = array(
                'Investment_distribution_Id' => $row['Investment_distribution_Id'],
                'fraction_investment_option' => $row['fraction_investment_option']
            );

            $obj = json_encode($jsonobj);
            array_push($transfers, $obj);
        }
    }

    $Investment_option_Id = 1;
        $datetime = date("Y-m-d H:i:s");
        $calculated_transfer_amount = 0;
        $transfer_amount= 0;
        $new_cash_amount = $old_cash_amount;
        $new_invested_amount = $old_invested_amount;
        $status = "out";
        $transfer_Id = 'generated';

    $sqlInsertTransferred = $conn->prepare("INSERT INTO `Transferred` (`Investment_option_Id`, `datetime`, `calculated_transfer_amount`, `transfer_amount`, `old_cash_amount`, `new_cash_amount`, `new_invested_amount`, `old_invested_amount`, `status`, `ideal_valuation`, `transfer_Id`) 
                                values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

    $sqlInsertTransferred->bind_param('isddddddsds',
        $Investment_option_Id,
        $datetime,
        $calculated_transfer_amount,
        $transfer_amount,
        $old_cash_amount,
        $new_cash_amount,
        $new_invested_amount,
        $old_invested_amount,
        $status,
        $ideal_valuation,
        $transfer_Id
    );
    $sqlInsertTransferred->execute();

    foreach ($transfers as &$transferEncoded) {
        $transfer = json_decode($transferEncoded);

        $sqlInsertValuation = $conn->prepare("INSERT into Valuation_contribution 
                                        (Investment_distribution_Id, Transferred_id, fraction_investment_option) 
                                        values(?, (select max(id) from Transferred), ?)");
        $Investment_distribution_Id = $transfer->Investment_distribution_Id;
        $fraction_investment_option = $transfer->fraction_investment_option;

        $sqlInsertValuation->bind_param('id',
            $Investment_distribution_Id,
            $fraction_investment_option);
        $sqlInsertValuation->execute();
    }

    $conn->close();
}
?>