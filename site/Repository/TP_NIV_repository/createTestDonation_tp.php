
<?php
    function createTestDonation($amount){

        $conn = OpenCon();

        $sqlInsertDonation = $conn->prepare("INSERT into wppx_charitable_campaign_donations (donation_id, donor_id, campaign_id, campaign_name, amount)
        values(1, 1, 1,'generated_donation', ?);");
        $sqlInsertDonation->bind_param('d',
            $amount
        );
        $sqlInsertDonation->execute();

        $sqlInsertValuation = $conn->prepare("INSERT into Investment_distribution (WPPX_charitable_campaign_donations_Id, Investment_option_id, Transferred_Id, fraction_distribution_donation)
        values((select max(campaign_donation_id) from wppx_charitable_campaign_donations), 1, 0, 1.0000000000)");
        $sqlInsertValuation->execute();

        $conn->close();
    }
?>