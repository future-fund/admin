<?php
function calcIdealVal_tp($fundId, $transWorthBefore, $transAmount, $old_cash_amount){
    $connCalcIdealVal = OpenCon();

    $sqlCalcIdealVal = $connCalcIdealVal->prepare("
    SELECT ((? + ?) / (new_invested_amount + new_cash_amount)
                * ideal_valuation - ideal_valuation)
            * (1-fraction_to_reinvest) + ideal_valuation + ? as amount
        FROM Transferred t
        JOIN Investment_option o on t.Investment_option_id = o.Id
        WHERE t.Investment_option_Id = ? and t.status = 'in' 
        ORDER BY t.datetime DESC
        LIMIT 1;");
    $sqlCalcIdealVal->bind_param('dddi', $transWorthBefore, $old_cash_amount, $transAmount, $fundId);

    $sqlCalcIdealVal->execute();
    $sqlCalcIdealVal->bind_result($calcIdealVal);
    $sqlCalcIdealVal->fetch();
    $connCalcIdealVal->close();
    if($calcIdealVal == null){
        return $transAmount;
    } else{
        return $calcIdealVal;
    }
}
?>
