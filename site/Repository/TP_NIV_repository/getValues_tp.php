<?php
include 'calcIdealVal_tp.php';

function getValues_tp($new_invested_amount, $new_cash_amount, $old_invested_amount){
    $conn = OpenCon();

    $sql = "select o.Id, o.Name, sum(cd.amount) as amount
            from Investment_option o join Investment_distribution d on o.Id = d.Investment_option_Id
            join wppx_charitable_campaign_donations cd on d.WPPX_charitable_campaign_donations_Id = cd.campaign_donation_id
            left join Valuation_contribution c on d.Id = c.Investment_distribution_Id
            where c.Id is null
            group by o.Id";

    $result = $conn->query($sql);

    $transfers = array();

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $Investment_option_Id = $row['Id'];
            $amount = $row['amount'];

            $old_cash_amount = null;
            $sql1 = $conn->prepare("select COALESCE(new_cash_amount, 0) as new_cash_amount
                        from Transferred 
                        where Investment_option_Id = ?
                        and Id = (select MAX(Id) from Transferred where Investment_option_Id = ?)");
            $sql1->bind_param("ii",$Investment_option_Id,$Investment_option_Id);
            $sql1->execute();
            $result1 = $sql1->get_result();
            
            if ($result1->num_rows > 0) {
                while ($row1 = $result1->fetch_assoc()) {
                    $old_cash_amount = $row1['new_cash_amount'];
                }
            }
            if($old_cash_amount == null){
                $old_cash_amount = 0;
            }

            $maxTransferredId = getMaxTransferredId_NIV_Repository($Investment_option_Id);
            $transfer_Id = "test_transfer_Id";
            $new_invested_amount = $new_invested_amount;
            $new_cash_amount = $new_cash_amount;
            $old_invested_amount = $old_invested_amount;
            $conversion_datetime = date("Y-m-d H:i:s");

            $calculated_transfer_amount = $amount  + $old_cash_amount;
            $transfer_amount = $calculated_transfer_amount - $new_cash_amount;

            $idealVal = calcIdealVal_tp($Investment_option_Id, $old_invested_amount, $amount, $old_cash_amount);

            $jsonobj = array('Investment_option_Id' => $Investment_option_Id,
                'amount' => $amount,
                'old_cash_amount' => $old_cash_amount,
                'new_invested_amount' => $new_invested_amount,
                'new_cash_amount' => $new_cash_amount,
                'old_invested_amount' => $old_invested_amount,
                'calculated_transfer_amount' => $calculated_transfer_amount,
                'transfer_Id' => $transfer_Id,
                'transfer_amount' => $transfer_amount,
                'conversion_datetime' => $conversion_datetime,
                'maxTransferredId' => $maxTransferredId,
                'ideal_valuation' => $idealVal);

            $obj = json_encode($jsonobj);
            array_push($transfers, $obj);
        }
    }
    $conn->close();

    return $transfers;
}
?>