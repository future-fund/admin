<?php
function getLastTransfer(){
    $conn = OpenCon();
    $oldDonations = "SELECT * FROM Transferred WHERE Id = (SELECT MAX(Id) FROM Transferred)";
    $result = $conn->query($oldDonations);

    $jsonobj = null;
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $jsonobj = array(
                'calculatedAmount' => $row['calculated_transfer_amount'],
                'transfer_amount' => $row['transfer_amount'],
                'old_cash_amount' => $row['old_cash_amount'],
                'new_cash_amount' => $row['new_cash_amount'],
                'new_invested_amount' => $row['new_invested_amount'],
                'old_invested_amount' => $row['old_invested_amount'],
                'ideal_valuation' => $row['ideal_valuation']
            );
        }
    }
    $conn->close();
    return json_encode($jsonobj);
}
?>