<?php
function getTotalTransferFraction_tp(){
    $conn = OpenCon();

    $sql = "select fraction_investment_option from Valuation_contribution where Transferred_id = (SELECT MAX(Id) as id FROM Transferred)";

    $result = $conn->query($sql);
    $total_fraction_transfer = 0;
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $fraction_investment_option = $row['fraction_investment_option'];
            $total_fraction_transfer = $total_fraction_transfer + $fraction_investment_option;
        }
    }
    return $total_fraction_transfer;
}
?>