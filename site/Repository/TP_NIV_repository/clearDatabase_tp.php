<?php
function clearDatabase(){
    $conn = OpenCon();

    $sql = $conn->prepare("DELETE FROM Transferred_charity_Investment_distribution;");
    $sql->execute();
    $sql = $conn->prepare("DELETE FROM Transferred_charity;");
    $sql->execute();
    $sql = $conn->prepare("DELETE FROM Transferred_charity_Transferred;");
    $sql->execute();
    $sql = $conn->prepare("DELETE FROM Investment_option_valuation;");
    $sql->execute();
    $sql = $conn->prepare("DELETE FROM Valuation_contribution;");
    $sql->execute();
    $sql = $conn->prepare("DELETE FROM Transferred;");
    $sql->execute();
    $sql = $conn->prepare("DELETE FROM Investment_distribution");
    $sql->execute();
    $sql = $conn->prepare("DELETE FROM wppx_charitable_campaign_donations");
    $sql->execute();

    $conn->close();
}
?>