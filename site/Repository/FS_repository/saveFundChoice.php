<?php
function saveFundChoice($donor_id, $fund_id){
    //save what fund is chosen by the donor (gets used in trigger at moment of donation)
    $connInsertValuation = OpenCon();
    $sqlInsertValuation = $connInsertValuation->prepare(" UPDATE wppx_charitable_donors
                                                                SET wppx_charitable_donors.selected_fund = ?
                                                                WHERE wppx_charitable_donors.donor_id = ?;");
    $sqlInsertValuation->bind_param('ii', $fund_id, $donor_id);
    $sqlInsertValuation->execute();
    $connInsertValuation->close();
}

