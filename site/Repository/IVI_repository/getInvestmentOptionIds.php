<?php
function getInvestmentOptionIds(){
    $conn = OpenCon();

    $sql = "SELECT Investment_option.Id FROM Investment_option";
    $result = $conn->query($sql);
    $ids=array();
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            array_push($ids, $row['Id']);
        }
    } else {
        echo "<script>alert('Er zijn geen investment funds die opgeslagen moeten worden' );</script>";
    }
    return $ids;
}
?>