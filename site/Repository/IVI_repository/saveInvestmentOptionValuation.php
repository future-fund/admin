<?php
function saveInvestmentOptionValuation($recordID, $datetime, $invested, $cash, $idealVal){
    $conn = OpenCon();
    $sql = $conn->prepare("insert into Investment_option_valuation (Investment_option_Id, Transferred_Id, refresh_datetime, Invested_amount, cash_amount, ideal_valuation, for_calculation) values (?, null, ?, ?, ?, ?, true)");
    $sql->bind_param("isddd",$recordID,$datetime,$invested,$cash,$idealVal);
    $sql->execute();
    $conn->close();
}
?>