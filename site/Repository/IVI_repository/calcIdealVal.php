<?php
function calcIdealVal($fundId, $transWorthBefore, $cashBefore){
    $connCalcIdealVal = OpenCon();

    $sqlCalcIdealVal = $connCalcIdealVal->prepare("
        SELECT COALESCE(((? + ?) / (Invested_amount + cash_amount)
            * ideal_valuation - ideal_valuation)
            * (1 - fraction_to_reinvest) + ideal_valuation, 0) as amount
        FROM Investment_option_valuation iov
        JOIN Investment_option i on iov.Investment_option_Id = i.Id
        WHERE iov.Investment_option_Id = ? and iov.for_calculation = true 
        ORDER BY iov.refresh_datetime DESC, iov.Id DESC
        LIMIT 1;");
                                                         
                                                
    $sqlCalcIdealVal->bind_param('ddi', $transWorthBefore,$cashBefore, $fundId);


    $sqlCalcIdealVal->execute();
    $sqlCalcIdealVal->bind_result($calcIdealVal);
    $sqlCalcIdealVal->fetch();
    $connCalcIdealVal->close();
    return $calcIdealVal;
}
?>