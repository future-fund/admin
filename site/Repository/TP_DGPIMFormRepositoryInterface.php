<?php
include 'TP_DGPIMForm_repository/createTransferTable_tp.php';
function TP_CreateTransferTable_DGIMPForm_Repository($fundId, $calcAmount, $invested, $wortAfter, $worthBefore){
    createTransferTable_tp($fundId, $calcAmount, $invested, $wortAfter, $worthBefore);
}

include 'TP_DGPIMForm_repository/createInvestment_option_valuation_tp.php';
function TP_createInvestment_option_valuation_DGIMPForm_Repository($fundId, $worthAfter){
    createInvestment_option_valuation_tp($fundId, $worthAfter);
}

include 'TP_DGPIMForm_repository/getTransferred_tp.php';
function TP_getTransferred_DGPIMForm_Repository(){
    return getNewestTransferred();
}

include 'TP_DGPIMForm_repository/getValuation_contribution_Rows_tp.php';
function TP_Valuation_contribution_Rows_DGPIMForm_Repository(){
    return getValuation_contribution_Rows();
}

include 'TP_DGPIMForm_repository/getTransferred_charity_Transferred_Rows_tp.php';
function TP_Transferred_charity_Transferred_Rows_DGPIMForm_Repository(){
    return getTransferred_charity_Transferred_Rows();
}

include 'TP_DGPIMForm_repository/getTransferred_charity_Investment_distribution_tp.php';
function TP_Transferred_charity_Investment_distribution_DGPIMForm_Repository(){
    return getTransferred_charity_Investment_distribution();
}

include 'TP_DGPIMForm_repository/getTransferred_charity_DGPIMForm_Repository_tp.php';
function TP_Transferred_charity_DGPIMForm_Repository(){
    return getTransferred_charity_DGPIMForm_Repository();
}

include 'TP_DGPIMForm_repository/getInvestment_option_valuation_DGPIMForm_Repository_tp.php';
function TP_Investment_option_valuation_DGPIMForm_Repository(){
    return getInvestment_option_valuation_DGPIMForm_Repository();
}
?>