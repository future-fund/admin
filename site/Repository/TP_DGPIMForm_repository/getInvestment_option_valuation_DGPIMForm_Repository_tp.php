<?php
function getInvestment_option_valuation_DGPIMForm_Repository()
{
    $conn = OpenCon();
    $oldDonations = "SELECT * FROM Investment_option_valuation where Investment_option_valuation.Id = (select max(iov.Id) from Investment_option_valuation iov)";
    $result = $conn->query($oldDonations);

    $jsonobj = null;
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $jsonobj = array(
                'investment_option' => $row['Investment_option_Id'],
                'Transferred_Id' => $row['Transferred_Id'],
                'invested_amount' => $row['Invested_amount'],
                'cash_amount' => $row['cash_amount'],
                'ideal_valuation' => $row['ideal_valuation'],
                'for_calculation' => $row['for_calculation'],
            );
        }
    }
    $conn->close();
    return json_encode($jsonobj);
}
