<?php
function getNewestTransferred()
{
    $conn = OpenCon();
    $oldDonations = "SELECT * FROM Transferred WHERE Id = (SELECT MAX(Id) FROM Transferred)";
    $result = $conn->query($oldDonations);

    $jsonobj = null;
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $jsonobj = array(
                'ideal_valuation' => $row['ideal_valuation']
            );
        }
    }
    $conn->close();
    return json_encode($jsonobj);
}
