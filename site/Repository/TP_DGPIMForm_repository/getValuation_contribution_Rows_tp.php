<?php
function getValuation_contribution_Rows()
{
    $conn = OpenCon();
    $sqlValuation_contribution_rows = "SELECT COUNT(Valuation_contribution.Id) as amount FROM Valuation_contribution";
    $resultValuation_contribution_rows = $conn->query($sqlValuation_contribution_rows);
    $conn->close();
    return $resultValuation_contribution_rows;
}
