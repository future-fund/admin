<?php

function createTransferTable_tp($fundId, $calcAmount, $invested, $wortAfter, $worthBefore)
{
    $connInsertTransferred = OpenCon();
    //sql query to create a new Transferred row
    $sqlInsertTransferred = $connInsertTransferred->prepare("INSERT INTO `Transferred` (`Investment_option_Id`, `datetime`, `calculated_transfer_amount`, `transfer_amount`, `old_cash_amount`, `new_cash_amount`, `new_invested_amount`, `old_invested_amount`, `status`, `ideal_valuation`, `transfer_Id`) 
            VALUES (?, ?, ?, ?,                
                (SELECT Investment_option_valuation.cash_amount 
                FROM Investment_option_valuation
                where Investment_option_valuation.Investment_option_Id = ? 
                ORDER BY Investment_option_valuation.refresh_datetime DESC
                LIMIT 1)
                , (SELECT Investment_option_valuation.cash_amount 
                FROM Investment_option_valuation 
                where Investment_option_valuation.Investment_option_Id = ? 
                ORDER BY Investment_option_valuation.refresh_datetime DESC
                LIMIT 1), ?, ?, 'out', ?, ?)");


    $transCalcAmount = $calcAmount;
    $transInvested = $invested;
    $transWorthAfter = $wortAfter;
    $transWorthBefore = $worthBefore;
    $datetime = "2021-01-12 16:48:41" ;
    $transferId = "test";
    $transIdealVal = calcIdealVal($fundId, $transWorthBefore);

    $sqlInsertTransferred->bind_param('isddiiddds',
        $fundId,
        $datetime,
        $transCalcAmount,
        $transInvested,
        $fundId,
        $fundId,
        $transWorthAfter,
        $transWorthBefore,
        $transIdealVal,
        $transferId
    );
    $sqlInsertTransferred->execute();
    $connInsertTransferred->close();
}

