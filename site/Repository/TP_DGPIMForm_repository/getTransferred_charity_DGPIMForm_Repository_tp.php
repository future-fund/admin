<?php
function getTransferred_charity_DGPIMForm_Repository()
{
    $conn = OpenCon();
    $oldDonations = "SELECT * FROM Transferred_charity";
    $result = $conn->query($oldDonations);

    $jsonobj = null;
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $jsonobj = array(
                'transfer_amount' => $row['transfer_amount'],
                'transfer_datetime' => $row['transfer_datetime'],
                'status' => $row['status'],
                'wppx_posts_Id' => $row['WPPX_posts_Id']
            );
        }
    }
    $conn->close();
    return json_encode($jsonobj);
}
