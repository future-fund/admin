<?php

function createInvestment_option_valuation_tp($fundId, $worthAfter)
{
    //Create a new C2 row for the fund
    $connInsertValuation = OpenCon();
    $sqlInsertValuation = $connInsertValuation->prepare("insert into Investment_option_valuation (Investment_option_Id, Transferred_Id, refresh_datetime, Invested_amount, cash_amount, ideal_valuation, for_calculation) 
                                                        values ( ?, 
                                                                (
                                                                    SELECT Transferred.Id 
                                                                    FROM Transferred
                                                                    where Transferred.Investment_option_Id = ? and Transferred.status = 'out' 
                                                                    ORDER BY Transferred.datetime DESC
                                                                    LIMIT 1
                                                                ), 
                                                                ?, 
                                                                ?,
                                                                (
                                                                    SELECT iov.cash_amount 
                                                                    FROM Investment_option_valuation as iov
                                                                    where iov.Investment_option_Id = ?
                                                                    ORDER BY iov.refresh_datetime DESC
                                                                    LIMIT 1
                                                                ),
                                                                (
                                                                    select Transferred.ideal_valuation from Transferred 
                                                                    where Transferred.Id = (select max(t.Id) from Transferred t)
                                                                ),
                                                                0
                                                            );
                                                        ");
    $datetime = '2021-01-13 13:24:28';
    $transWorthAfter = $worthAfter;


    $sqlInsertValuation->bind_param('iisdi', $fundId, $fundId, $datetime, $transWorthAfter, $fundId);
    $sqlInsertValuation->execute();
    $connInsertValuation->close();
}

