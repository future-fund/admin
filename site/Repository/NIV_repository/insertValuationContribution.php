<?php
function insertValuationContribution($transfers, $transferredId){
    $conn = OpenCon();

    foreach ($transfers as &$transferEncoded) {
        $transfer = json_decode($transferEncoded);

        $sqlInsertValuation = $conn->prepare("INSERT into Valuation_contribution 
                                        (Investment_distribution_Id, Transferred_id, fraction_investment_option) 
                                        values(?, ?, ?)");
        $Investment_distribution_Id = $transfer->Investment_distribution_Id;
        $fraction_investment_option = $transfer->fraction_investment_option;

        $sqlInsertValuation->bind_param('iid',
            $Investment_distribution_Id,
            $transferredId,
            $fraction_investment_option);

        $sqlInsertValuation->execute();
    }
    $conn->close();
}
?>