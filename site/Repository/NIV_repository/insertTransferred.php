<?php
function insertTransferred($transfer){
    $conn = OpenCon();

    $insert = $conn->prepare("INSERT into Transferred (Investment_option_Id, 
                                    datetime, 
                                    calculated_transfer_amount, 
                                    transfer_amount, 
                                    old_cash_amount, 
                                    new_cash_amount, 
                                    new_invested_amount, 
                                    old_invested_amount, 
                                    status, 
                                    ideal_valuation, 
                                    transfer_Id) 
    values (?, ?, ?, ?, ?, ?, ?, ?, 'in', ?, ?)");

    $insert->bind_param('isdddddddi',
        $transfer->Investment_option_Id,
        $transfer->conversion_datetime,
        $transfer->calculated_transfer_amount,
        $transfer->transfer_amount,
        $transfer->old_cash_amount,
        $transfer->new_cash_amount,
        $transfer->new_invested_amount,
        $transfer->old_invested_amount,
        $transfer->ideal_valuation,
        $transfer->transfer_Id
    );

    $insert->execute();
    $conn->close();
}
?>