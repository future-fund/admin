<?php
function getNewFractionPerOldDonation($maxTransferredId, $oldTotalVal, $newTotalVal){
    $transfers = array();
    // percentage dat de oude donaties uit maken van de nieuwe investeringshoeveelheid
    if($oldTotalVal != 0 && $maxTransferredId != null) {
        $newFractionOldDonations = (($oldTotalVal * 100) / $newTotalVal) / 100;
        $conn = OpenCon();
        $oldDonations = $conn->prepare("SELECT c.fraction_investment_option, c.Investment_distribution_Id
            FROM Transferred t join Valuation_contribution c on t.Id = c.Transferred_id
            WHERE t.Id = ?");
        $oldDonations->bind_param("i",$maxTransferredId);
        $result = $oldDonations->get_result();

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $fraction_investment_optioncalculated = (floatval($row['fraction_investment_option']) * $newFractionOldDonations) / 1;
                $Investment_distribution_Id = $row['Investment_distribution_Id'];

                $jsonobj = array('Investment_distribution_Id' => $Investment_distribution_Id,
                    'fraction_investment_option' => $fraction_investment_optioncalculated);

                $obj = json_encode($jsonobj);
                array_push($transfers, $obj);
            }
        }
        $conn->close();
    }
    return $transfers;
}
?>