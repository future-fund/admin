<?php
function getFractionNewDonations($Investment_option_Id, $oldTotalVal, $newTotalVal, $old_contributions){
    // percentage dat de oude donaties uit maken van de nieuwe investeringshoeveelheid
    $addVal = $newTotalVal - $oldTotalVal;
    $newFreqNewDonations = (($addVal * 100) / $newTotalVal) / 100;

    $conn = OpenCon();
    $sql =  $conn->prepare("SELECT d.Id, a.amount
            from Investment_option o join Investment_distribution d on o.Id = d.Investment_option_Id
            left join Valuation_contribution c on d.Id = c.Investment_distribution_Id
            join wppx_charitable_campaign_donations a on d.WPPX_charitable_campaign_donations_Id = a.campaign_donation_id
            where o.Id = ?
            AND c.Id is null");
    $sql->bind_param("i",$Investment_option_Id);
    $sql->execute();
    $result = $sql->get_result();

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $amount = floatval($row['amount']);
            $fraction_investment_option = ($newFreqNewDonations * $amount) / $addVal;

            $jsonobj = array('Investment_distribution_Id' => $row['Id'],
                'fraction_investment_option' => $fraction_investment_option);

            $obj = json_encode($jsonobj);
            array_push($old_contributions, $obj);
        }
    }
    $conn->close();

    return $old_contributions;
}
?>