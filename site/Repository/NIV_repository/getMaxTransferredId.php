<?php
function getMaxTransferredId($Investment_option_Id){
    $conn = OpenCon();
    $oldDonations = $conn->prepare("SELECT MAX(a.Id) as id FROM Transferred as a WHERE a.Investment_option_Id = ?");
    $oldDonations->bind_param("i",$Investment_option_Id);
    $oldDonations->execute();
    $result = $oldDonations->get_result();

    $id = null;
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $id = $row['id'];
        }
    }
    $conn->close();
    return $id;
}
?>