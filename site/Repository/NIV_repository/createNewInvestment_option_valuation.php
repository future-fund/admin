<?php
function createNewInvestment_option_valuation($Investment_option_Id, $cash_amount, $refresh_datetime, $Invested_amount, $Transferred_Id){
    $conn = OpenCon();

    $sqlInsertValuation = $conn->prepare("INSERT into Investment_option_valuation 
                                        (Investment_option_Id, refresh_datetime, Invested_amount, Transferred_Id, cash_amount, ideal_valuation, for_calculation) 
                                        values(?, ?, ?, ?, ?,
                                                                (
                                                                    select Transferred.ideal_valuation from Transferred 
                                                                    where Transferred.Id = (select max(t.Id) from Transferred t)
                                                                ),
                                                                1)");

    $sqlInsertValuation->bind_param('isdid',
        $Investment_option_Id,
        $refresh_datetime,
        $Invested_amount,
        $Transferred_Id,
        $cash_amount);

    $sqlInsertValuation->execute();

    $conn->close();
}
?>