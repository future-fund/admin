<?php
function getnew_invested_amountTransferIdByInvestmentOption($Investment_option_Id){
    $conn = OpenCon();

    $sql = $conn->prepare("SELECT Id, new_invested_amount, new_cash_amount FROM Transferred 
            WHERE Id = (SELECT MAX(Id) FROM Transferred WHERE Investment_option_Id = ?)");
    $sql->bind_param("i",$Investment_option_Id);
    $sql->execute();
    $result = $sql->get_result();
    
    $transferredId = null;

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $transferredId =  $row['Id'];
        }
    }
    $conn->close();
    return $transferredId;
}
?>