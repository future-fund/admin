<?php 

/**
 * Prepare child theme. 
 *
 * This enqueues the child theme's style.css file after first loading
 * the main theme's stylesheet.
 *
 * @return  void
 * @since   1.0.0
 */
function reach_child_load_styles() {    
    wp_register_style( 'reach-child-styles', get_stylesheet_directory_uri() . "/style.css", array( 'reach-style' ), reach_get_theme()->get_theme_version() );
    wp_enqueue_style( 'reach-child-styles' );
}

add_action( 'wp_enqueue_scripts', 'reach_child_load_styles', 100 );